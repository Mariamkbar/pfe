import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router,Route } from 'react-router-dom';

import App from "./App";
import * as serviceWorker from "./serviceWorker";
import Menu from "./Menu";
import Footer from "./components/Footer";
import CreatePatient from "./components/Patient/CreatePatient";
import EditPatient from "./components/Patient/EditPatient";
import DossierMDicale from "./components/Patient/DossierMédicale";
import Consultation from "./components/Consultation/Consultation";
import ConsultationList from "./components/Consultation/ConsultationList";
import Medecin from "./components/Medecins/Medecin";
import MedecinList from "./components/Medecins/MedecinList";
import Drugs from './components/Drugs/Drugs';
import DrugList from './components/Drugs/DrugList';
import CreatePrescribe from "./components/Drugs/Prescribe";
import PrescribeList from "./components/Drugs/PrescribeList";
import Examen from './components/Examens/Examen';
import ExamensList from './components/Examens/ExamensList';
import CreateConducted_physical_exam from "./components/Examens/Conducted_physical_exam";
import ResultatExamList from "./components/Examens/ResultatExamList";
import Maladies from './components/Maladies/Maladies';
import MaladiesList from './components/Maladies/MaladiesList';
import CreateOrgan from "./components/Maladies/CreateOrgan"
import OrganList from "./components/Maladies/OrganList";
import CreateDiagnostic from "./components/Maladies/Diagnostic";
import DiagnosticList from "./components/Maladies/DiagnosticList";
import EditConsultation from "./components/Consultation/EditConsultation";
import PatientListe from "./components/Patient/PatientListe";
import OrganEdit from "./components/Maladies/OrganEdit";
import EditExam from "./components/Examens/EditExam";
import MaladyEdit from "./components/Maladies/MaladyEdit";
import EditExamResult from "./components/Examens/EditConducted_physical_exam";
import EditDiagnostic from "./components/Maladies/EditDiagnostic";
import EditMedecin from "./components/Medecins/EditMedecin";
import EditDrog from "./components/Drugs/EditDrog";
import EditPrescribe from "./components/Drugs/EditPrescribe";
import ordonnancePDF from "./components/Drugs/OrdonnancePDF";
import SimpleConsultation from "./components/Consultation/SimpleConsultation";
import ConsultationExamen from "./components/Consultation/ConsultationExamen";
import ConsultationOrdonnance from "./components/Consultation/ConsultationOrdonnance";
import ConsultaionsDiagnostique from "./components/Consultation/ConsultaionsDiagnostique";
import ExamenPDF from "./components/Examens/ExamenPDF";
import DiagnostiquesPDF from "./components/Maladies/DiagnostiquesPDF";


ReactDOM.render(
    <Router>
        <App />
        <Menu/>
        <div>
            <Route path='/createp' component={CreatePatient} />
            <Route path='/listpas' component={PatientListe} />
            <Route path='/edit/:id' component={EditPatient} />
            <Route path='/show/:id' component={DossierMDicale} />
            <Route exact path='/addcons' component={Consultation} />
            <Route exact path='/addScons' component={SimpleConsultation} />
            <Route exact path='/addEcons' component={ConsultationExamen} />
            <Route exact path='/addOcons' component={ConsultationOrdonnance} />
            <Route exact path='/addDcons' component={ConsultaionsDiagnostique} />
            <Route exact path='/listcons' component={ConsultationList} />
            <Route path='/addmedecins' component={Medecin} />
            <Route path='/listmedecins' component={MedecinList} />
            <Route path='/adddrugs' component={Drugs} />
            <Route path='/listdrugs' component={DrugList} />
            <Route path='/adddpr' component={CreatePrescribe} />
            <Route path='/listpr' component={PrescribeList} />
            <Route path='/addexamans' component={Examen} />
            <Route path='/listexam' component={ExamensList} />
            <Route path='/adddcpe' component={CreateConducted_physical_exam} />
            <Route path='/addres' component={ResultatExamList} />
            <Route path='/addmaladies' component={Maladies} />
            <Route path='/listmaladies' component={MaladiesList} />
            <Route path='/adddorgan' component={CreateOrgan} />
            <Route path='/editorg/:id' component={OrganEdit} />
            <Route path='/editexamans/:id' component={EditExam} />
            <Route path='/editmaladies/:id' component={MaladyEdit} />
            <Route path='/editexamresult/:id' component={EditExamResult} />
            <Route path='/editdiagnostic/:id' component={EditDiagnostic} />
            <Route path='/editmedecin/:id' component={EditMedecin} />
            <Route path='/editdrog/:id' component={EditDrog} />
            <Route path='/editordo/:id' component={EditPrescribe} />
            <Route path='/listorgan' component={OrganList} />
            <Route path='/adddg' component={CreateDiagnostic} />
            <Route path='/listDiag' component={DiagnosticList} />
            <Route path='/editcon/:id' component={EditConsultation} />
            <Route path='/pdf/:id' component={ordonnancePDF} />
            <Route path='/pdfEX/:id' component={ExamenPDF} />
            <Route path='/pdfDig/:id' component={DiagnostiquesPDF} />

        </div>
        <Footer/>
    </Router>,
    document.getElementById("root")
);

serviceWorker.unregister();
