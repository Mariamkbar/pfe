import http from "../http-common";

const getAll = () => {
    return http.get("/organs");
};

const get = id => {
    return http.get(`/organs/${id}`);
};

const create = data => {
    return http.post("/organs", data);
};

const update = (id, data) => {
    return http.put(`/organs/${id}`, data);
};

const remove = id => {
    return http.delete(`/organs/${id}`);
};

const removeAll = () => {
    return http.delete(`/organs`);
};

const findByTitle = title => {
    return http.get(`/organs?title=${title}`);
};

export default {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll,
    findByTitle
};

