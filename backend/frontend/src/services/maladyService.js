import http from "../http-common";

const getAll = () => {
    return http.get("/maladies");
};

const get = id => {
    return http.get(`/maladies/${id}`);
};

const create = data => {
    return http.post("/maladies", data);
};

const update = (id, data) => {
    return http.put(`/maladies/${id}`, data);
};

const remove = id => {
    return http.delete(`/maladies/${id}`);
};

const removeAll = () => {
    return http.delete(`/maladies`);
};

const findByTitle = title => {
    return http.get(`/maladies?title=${title}`);
};

export default {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll,
    findByTitle
};

