import http from "../http-common";

const getAll = () => {
    return http.get("/physical_exams");
};

const get = id => {
    return http.get(`/physical_exams/${id}`);
};

const create = data => {
    return http.post("/physical_exams", data);
};

const update = (id, data) => {
    return http.put(`/physical_exams/${id}`, data);
};

const remove = id => {
    return http.delete(`/physical_exams/${id}`);
};

const removeAll = () => {
    return http.delete(`/physical_exams`);
};

const findByTitle = title => {
    return http.get(`/physical_exams?title=${title}`);
};

export default {
    getAll,
    get,
    create,
    update,
    remove,
    removeAll,
    findByTitle
};