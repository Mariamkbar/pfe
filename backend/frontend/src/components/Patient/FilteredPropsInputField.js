import React from "react";
import { Field } from "formik";

//Validatins des inputs

function FilteredPropsInputField({ className, valid, error, ...props }) {
    return <Field className={className} {...props} />;
}

export default FilteredPropsInputField;
