import React, { Component } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faList,
  faTrash,
  faPen,
  faHospitalUser,
  faFolderOpen,
  faFilePrescription, faXRay, faDiagnoses
} from '@fortawesome/free-solid-svg-icons';




import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css'
import './Style.css';
import { Card, Row, Col, Button } from 'react-bootstrap';
import ReactPaginate from "react-paginate";






class DossierMDicale extends Component {

  constructor(props) {
    super(props);
    this.state = {
      patient: {
        image: { data: "" }
      },
      consultations: [],
      patients: [],
      offset: 0,
      orgtableData: [],
      perPage: 3,
      currentPage: 0
    };
    this.handlePageClick = this.handlePageClick.bind(this);
  }

  componentDidMount() {
    this.getConsultation();
    axios.get('/patients/' + this.props.match.params.id)
      .then(res => {
        this.setState({ patient: res.data });
        console.log(this.state.patient);

      });

    this.getData();



  }


  handlePageClick = (e) => {
    const selectedPage = e.selected;
    const offset = selectedPage * this.state.perPage;

    this.setState(
      {
        currentPage: selectedPage,
        offset: offset
      },
      () => {
        this.loadMoreData();
      }
    );
  };

  loadMoreData() {
    const data = this.state.orgtableData;

    const slice = data.slice(
      this.state.offset,
      this.state.offset + this.state.perPage
    );
    this.setState({
      pageCount: Math.ceil(data.length / this.state.perPage),
      consultations: slice
    });
  }
  getData() {
    axios.get('/consultations/getConsultationByPatientId/' + this.props.match.params.id).then((res) => {
      this.setState({ consultations: res.data });
      console.log(this.state.consultations);
      const data = res.data;

      const slice = data.slice(
        this.state.offset,
        this.state.offset + this.state.perPage
      );

      this.setState({
        pageCount: Math.ceil(data.length / this.state.perPage),
        orgtableData: res.data,
        consultations: slice
      });
    });
  }


  getConsultation() {

    axios.get('/consultations/getConsultationByPatientId/' + this.props.match.params.id)
      .then(res => {
        this.setState({ consultations: res.data });
        console.log(this.state.consultations);
      });

  }



  delete(id) {
    console.log(id);
    if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?')) {
      axios.delete('/patients/' + id)
        .then((result) => {
          if (result.data != null) {
            alert('Patient été suprimer!!');
          }
          this.props.history.push("/createp")

        });


    }

  }

  PatientListe = () => {
    return this.props.history.push("/listpas");
  };






  render() {



    return (
      <div className="container">
        <div className="content-wrapper">
          <div>
            <div className="header bg-primary pb-6">
              <div className="container-fluid">
                <div className="header-body">
                  <div className="row align-items-center py-4">
                    <div className="col-lg-6 col-7">
                      <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faHospitalUser} /> Gestion des Patients</h6>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* Page content */}
            <div className="container-fluid mt--6">
              <div className="row">
                <div className="col">
                  <div className="card">
                    {/* Card header */}
                    <div className="card-header border-0">
                      <h3 className="mb-0"><FontAwesomeIcon icon={faFolderOpen} />  Dossier Médical du Patient
                        </h3>
                    </div>
                    <div className="card-body bg-white">
                      <Col className={"border border-white "} style={{ "width": "100%", "height": "100%" }}>
                        <Row>
                          <Col style={{ "margin": "auto" }}>
                            <dl>
                              <dt>Nom: {this.state.patient.nom}</dt>
                              <dt>Prenom: {this.state.patient.prenom}</dt>


                              <dt>Date de Naissannce: {this.state.patient.birth_date}</dt>


                            </dl>
                          </Col>
                          <Col>
                            <div style={{ "width": "70%", "height": "70%", "marginRight": "30%", "marginLeft": "30%" }}>
                              <img class="rounded-circle" src={`data:image/png;base64,${this.state.patient.image.data}`} />

                            </div>
                          </Col>
                        </Row>
                      </Col>
                      <Card className="pinf1" style={{ "width": "100%", "margin": "auto" }} >

                        <Tabs defaultIndex={1} onSelect={index => console.log(index)}>
                          <TabList>
                            <Tab>Fiche</Tab>
                            <Tab>Consultations</Tab>
                            <Tab>Examens & Résultats</Tab>
                            <Tab>Maladies & Diagnostiques</Tab>
                            <Tab>Medecins</Tab>
                            <Tab>Médicaments & Ordonnance</Tab>


                          </TabList>
                          <TabPanel>
                            <dt>Sexe: {this.state.patient.sexe}</dt>
                            <dt>Groupe Sanguin: {this.state.patient.groupe_sanguin}</dt>
                            <dt>Lieu de Naissannce: {this.state.patient.birth_place}</dt>
                            <dt>Birth Rank: {this.state.patient.birth_rank}</dt>
                            <dt>Nationalite: {this.state.patient.nationalite}</dt>
                            <dt>Profession: {this.state.patient.profession}</dt>
                            <dt>Ville :{this.state.patient.ville}</dt>
                            <dt>Pays: {this.state.patient.contry}</dt>
                            <dt>Adress 1: {this.state.patient.adress_1}</dt>
                            <dt>Adress 2: {this.state.patient.adress_2}</dt>
                            <dt>Telephone 1: {this.state.patient.tel1}</dt>
                            <dt>Telephone 2: {this.state.patient.tel2}</dt>
                            <dt>Whatapp : {this.state.patient.whatapp}</dt>
                            <dt>Wire: {this.state.patient.wire}</dt>



                            <br></br><br></br><br></br><br></br>
                            <br></br><br></br>

                          </TabPanel>
                          <TabPanel>
                            <div className="table-responsive">
                              <table className="table table-stripe">
                                <thead className="thead-light">
                                  <tr>
                                    <th scope="col" className="sort" data-sort="budget">titre</th>
                                    <th scope="col" className="sort" data-sort="status">observation</th>
                                    <th scope="col" className="sort" data-sort="status">date</th>
                                    <th scope="col" className="sort" data-sort="completion">cost</th>
                                    <th scope="col" className="sort" data-sort="completion">diagnostic</th>
                                    <th scope="col" className="sort" data-sort="completion">Ville</th>
                                    <th scope="col" className="sort" data-sort="completion">duration</th>
                                    <th scope="col" />
                                  </tr>
                                </thead>
                                <tbody className="list">
                                  {this.state.consultations?.map(c =>
                                    <tr>
                                      <td>{c.titre}</td>
                                      <td>{c.observation}</td>
                                      <td>{c.date}</td>
                                      <td>{c.cost}</td>
                                      <td>{c.diagnostic}</td>
                                      <td>{c.status}</td>
                                      <td>{c.duration}</td>


                                    </tr>
                                  )}


                                </tbody>


                              </table>
                              <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"}
                              />
                            </div>


                          </TabPanel>
                          <TabPanel>
                            <div className="table-responsive">
                              <table className="table table-stripe">
                                <thead className="thead-light">
                                  <tr>
                                    <th scope="col" className="sort" data-sort="completion">Examen Nom</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Catagory</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Sub Catagory</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Description</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Unit</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Unit Label</th>
                                    <th scope="col" className="sort" data-sort="completion">Examen Unit Type</th>
                                    <th scope="col" className="sort" data-sort="completion">Organ</th>
                                    <th scope="col" className="sort" data-sort="budget"> Resultat </th>
                                    <th scope="col" className="sort" data-sort="status">Resultat Comment </th>
                                    <th scope="col" className="sort" data-sort="status">Consultation Titre</th>
                                    <th scope="col" />
                                  </tr>
                                </thead>
                                <tbody className="list">
                                  {this.state.consultations?.map(c =>
                                    <tr>
                                      <td>{c.conducted_physical_exam?.physical_exam?.nom}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.catagory}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.sub_catagory}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.description}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.unit}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.unit_label}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.unit_type}</td>
                                      <td>{c.conducted_physical_exam?.physical_exam?.organ.nom}</td>
                                      <td>{c.conducted_physical_exam?.result}</td>
                                      <td>{c.conducted_physical_exam?.comment}</td>
                                      <td>{c.titre}</td>



                                    </tr>
                                  )}

                                </tbody>


                              </table>
                              <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"}
                              />
                            </div>
                          </TabPanel>

                          <TabPanel>
                            <div className="table-responsive">
                              <table className="table table-stripe">
                                <thead className="thead-light">
                                  <tr>
                                    <th scope="col" className="sort" data-sort="completion">malady Nom</th>
                                    <th scope="col" className="sort" data-sort="completion">malady Code</th>
                                    <th scope="col" className="sort" data-sort="completion">malady Catagory</th>
                                    <th scope="col" className="sort" data-sort="completion">malady Description</th>
                                    <th scope="col" className="sort" data-sort="completion">malady Sub Catagory</th>
                                    <th scope="col" className="sort" data-sort="budget"> Diagnostic Comment </th>
                                    <th scope="col" className="sort" data-sort="status">Consultation Titre</th>
                                    <th scope="col" />
                                  </tr>
                                </thead>
                                <tbody className="list">
                                  {this.state.consultations?.map(c =>
                                    <tr>
                                      <td>{c.diagnostics?.malady?.nom}</td>
                                      <td>{c.diagnostics?.malady?.code}</td>
                                      <td>{c.diagnostics?.malady?.catagory}</td>
                                      <td>{c.diagnostics?.malady?.description}</td>
                                      <td>{c.diagnostics?.malady?.sub_catagory}</td>
                                      <td>{c.diagnostics?.comment}</td>
                                      <td>{c.titre}</td>



                                    </tr>
                                  )}

                                </tbody>


                              </table>
                              <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"}
                              />
                            </div>
                          </TabPanel>
                          <TabPanel>

                            <div className="table-responsive">
                              <table className="table table-stripe">
                                <thead className="thead-light">
                                  <tr>
                                    <th scope="col" className="sort" data-sort="name">Consultation Titre</th>
                                    <th scope="col" className="sort" data-sort="name">Medecin Nom</th>
                                    <th scope="col" className="sort" data-sort="name">Medecin Specialite</th>
                                    <th scope="col" className="sort" data-sort="name">Medecin Address</th>
                                    <th scope="col" className="sort" data-sort="name">Medecin Téléphone</th>
                                    <th scope="col" />
                                  </tr>
                                </thead>
                                <tbody className="list">
                                  {this.state.consultations?.map(c =>
                                    <tr>
                                      <td>{c.titre}</td>
                                      <td>{c.medecin?.nom}</td>
                                      <td>{c.medecin?.specialite}</td>
                                      <td>{c.medecin?.address}</td>
                                      <td>{c.medecin?.tel}</td>



                                    </tr>
                                  )}


                                </tbody>


                              </table>
                              <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"}
                              />
                            </div>
                          </TabPanel>
                          <TabPanel>
                            <div className="table-responsive">
                              <table className="table table-stripe">
                                <thead className="thead-light">
                                  <tr>
                                    <th scope="col" className="sort" data-sort="completion">Drug Nom</th>
                                    <th scope="col" className="sort" data-sort="completion">Drug Brand</th>
                                    <th scope="col" className="sort" data-sort="completion">Drug Indication</th>
                                    <th scope="col" className="sort" data-sort="completion">Drug Contraindication</th>
                                    <th scope="col" className="sort" data-sort="budget"> PrescribeDosage</th>
                                    <th scope="col" className="sort" data-sort="status">Prescribe Duration</th>
                                    <th scope="col" className="sort" data-sort="status">Consultation Titre</th>
                                    <th scope="col" />
                                  </tr>
                                </thead>
                                <tbody className="list">
                                  {this.state.consultations?.map(c =>
                                    <tr>
                                      <td><ul>{c.prescribe?.drugs?.map((item)=>
                                          <li>
                                            {item.nom}
                                          </li>

                                      )}</ul></td>
                                      <td><ul>{c.prescribe?.drugs?.map((item)=>
                                          <li>
                                            {item.brand}
                                          </li>

                                      )}</ul></td>
                                      <td><ul>{c.prescribe?.drugs?.map((item)=>
                                          <li>
                                            {item.indication}
                                          </li>

                                      )}</ul></td>
                                      <td><ul>{c.prescribe?.drugs?.map((item)=>
                                          <li>
                                            {item.contraindication}
                                          </li>

                                      )}</ul></td>
                                      <td>{c.prescribe?.dosage}</td>
                                      <td>{c.prescribe?.duration}</td>
                                      <td>{c.titre}</td>



                                    </tr>
                                  )}

                                </tbody>


                              </table>
                              <ReactPaginate
                                previousLabel={"prev"}
                                nextLabel={"next"}
                                breakLabel={"..."}
                                breakClassName={"break-me"}
                                pageCount={this.state.pageCount}
                                marginPagesDisplayed={2}
                                pageRangeDisplayed={5}
                                onPageChange={this.handlePageClick}
                                containerClassName={"pagination"}
                                subContainerClassName={"pages pagination"}
                                activeClassName={"active"}
                              />
                            </div>
                          </TabPanel>



                        </Tabs>


                      </Card>






                      <Card.Footer style={{ "textAlign": "right" }}>
                        <Link to={`/edit/${this.state.patient.id}`} class="btn btn-success"
                          style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                          <FontAwesomeIcon icon={faPen} /> Modifier</Link>&nbsp;{' '}
                        <Link to={`/pdf/${this.state.patient.id}`} class="btn btn-info"
                          style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                          <FontAwesomeIcon icon={faFilePrescription} /> Ordonnances</Link>&nbsp;{' '}
                        <Link to={`/pdfEX/${this.state.patient.id}`} class="btn btn-info"
                              style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                          <FontAwesomeIcon icon={faXRay} /> Résultats</Link>&nbsp;{' '}
                        <Link to={`/pdfDig/${this.state.patient.id}`} class="btn btn-info"
                              style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                          <FontAwesomeIcon icon={faDiagnoses} /> Diagnostics</Link>&nbsp;{' '}
                        {/*   <button onClick={this.delete.bind(this, this.state.patient.id)}
                          class="btn btn-danger" style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                          <FontAwesomeIcon icon={faTrash} /> Supprimer</button>{' '}*/}

                        <Button size="sm" variant="info" type="button" onClick={this.PatientListe.bind()}>
                          <FontAwesomeIcon icon={faList} /> liste des Patients
                          </Button>{' '}

                      </Card.Footer>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}

export default DossierMDicale;
