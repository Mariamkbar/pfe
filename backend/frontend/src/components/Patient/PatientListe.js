import React, { PureComponent, Component } from 'react';
//import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import axios from 'axios';
import ReactPaginate from "react-paginate";

import { Card, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlusSquare, faList, faHospitalUser } from '@fortawesome/free-solid-svg-icons';
import "./styll.css";

//Component Liste des patient

class PatientListe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patients: [],
            offset: 0,
            orgtableData: [],
            perPage: 2,
            currentPage: 0


        };
        this.handlePageClick = this.handlePageClick.bind(this);
    }
    //Méthode bouton pagination
    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState(
            {
                currentPage: selectedPage,
                offset: offset
            },
            () => {
                this.loadMoreData();
            }
        );
    };
    //Méthode charger plus de données
    loadMoreData() {
        const data = this.state.orgtableData;

        const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
        );
        this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            patients: slice
        });
    }
    //Méthode obtenir des données
    getData() {
        axios.get(`/patients`).then((res) => {
            this.setState({ patients: res.data });
            console.log(this.state.patients);
            const data = res.data;

            const slice = data.slice(
                this.state.offset,
                this.state.offset + this.state.perPage
            );

            this.setState({
                pageCount: Math.ceil(data.length / this.state.perPage),
                orgtableData: res.data,
                patients: slice
            });
        });
    }


    componentDidMount() {

        this.getData();
    }
    //Méthode ajouter Patient
    CreatePatient = () => {
        return this.props.history.push("/createp");
    };

    render() {
        return (
            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faHospitalUser} /> Gestion des Patients</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Patients <Button size="sm" variant="info" type="button" style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }} onClick={this.CreatePatient.bind()}>
                                            <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Patient
                                        </Button></h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        {/* table des patient */}
                                        <table class="table table-stripe" >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort" data-sort="name">Image</th>
                                                    <th scope="col" className="sort" data-sort="name">Numero</th>
                                                    <th scope="col" className="sort" data-sort="name">Code</th>
                                                    <th scope="col" className="sort" data-sort="budget">Nom</th>
                                                    <th scope="col" className="sort" data-sort="status">Prenom</th>
                                                    <th scope="col" className="sort" data-sort="status">Sexe</th>
                                                    <th scope="col" className="sort" data-sort="completion">Date de Naissance</th>
                                                    <th scope="col" className="sort" data-sort="completion">Lieu de Naissance </th>
                                                    <th scope="col" className="sort" data-sort="completion">Nationalite </th>
                                                    <th scope="col" className="sort" data-sort="completion">Ville </th>
                                                    <th scope="col" className="sort" data-sort="completion">Contry </th>
                                                    <th scope="col" className="sort" data-sort="completion">Adress 1 </th>
                                                    <th scope="col" className="sort" data-sort="completion">Adress 2 </th>
                                                    <th scope="col" className="sort" data-sort="completion">Tel 1 </th>
                                                    <th scope="col" className="sort" data-sort="completion">Tel 2</th>
                                                    <th scope="col" className="sort" data-sort="completion">Groupe Sanguin </th>
                                                    <th scope="col" className="sort" data-sort="completion">Profession </th>
                                                    <th scope="col" className="sort" data-sort="completion">Whatapp </th>
                                                    <th scope="col" className="sort" data-sort="completion">Wire </th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">
                                                {this.state.patients.map(c =>
                                                    <tr><td >
                                                        <img className="rounded-circle"
                                                            width="100" heigh="100"
                                                            src={`data:image/png;base64,${c.image.data}`} /></td>
                                                        <td><Link to={`/show/${c.id}`}>{c.nom}</Link></td>
                                                        <td>{c.prenom}</td>
                                                        <td>{c.sexe}</td>
                                                        <td>{c.birth_date}</td>
                                                        <td>{c.birth_place}</td>
                                                        <td>{c.nationalite}</td>
                                                        <td>{c.ville}</td>
                                                        <td>{c.contry}</td>
                                                        <td>{c.adress_1}</td>
                                                        <td>{c.adress_2}</td>
                                                        <td>{c.tel1}</td>
                                                        <td>{c.tel2}</td>
                                                        <td>{c.groupe_sanguin}</td>
                                                        <td>{c.profession}</td>
                                                        <td>{c.whatapp}</td>
                                                        <td>{c.wire}</td>



                                                    </tr>


                                                )}


                                            </tbody>




                                        </table>


                                    </div>
                                    {/*Pagination */}

                                    <ReactPaginate
                                        previousLabel={"prev"}
                                        nextLabel={"next"}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        pageCount={this.state.pageCount}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={this.handlePageClick}
                                        containerClassName={"pagination"}
                                        subContainerClassName={"pages pagination"}
                                        activeClassName={"active"}
                                    />

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PatientListe;
