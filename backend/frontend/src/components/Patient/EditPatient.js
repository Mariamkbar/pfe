import React, { Component } from 'react';
import axios from 'axios';



import { Card, Button, Container, Col, Row, Form } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faEdit, faFolderOpen } from '@fortawesome/free-solid-svg-icons';
import { confirmAlert } from "react-confirm-alert";
//Component Modifier Patient
class EditPatient extends Component {

  constructor(props) {
    super(props);
    //les attribus
    this.state = {
      patient: {
        image: { data: "" }
      },
      image: '',
      images: null,
      ErrorStatus: ''
    };
    this.handleFile = this.handleFile.bind(this);
  }

  componentDidMount() {
    axios.get('/patients/' + this.props.match.params.id)
      .then(res => {
        this.setState({ patient: res.data });
        console.log(this.state.patient);
      });
  }

  //fuction qui gére l'image


  handleFile(e) {
    this.setState({ images: e.target.files[0] });
    this.setState({ image: '' });
    let reader = new FileReader();
    let images = e.target.files[0];
    reader.onloadend = () => {
      this.setState({ file: images, imagePreviewUrl: reader.result });
    }
    reader.readAsDataURL(images)

  }

  //Méthode Onchange inputs

  onChange = (e) => {
    const state = this.state.patient
    state[e.target.name] = e.target.value;
    this.setState({ patient: state });
  }

  //Méthode put Data

  onSubmit = (e) => {
    e.preventDefault();
    const {  nom, prenom, sexe, groupe_sanguin, birth_date,
      birth_place, birth_rank, nationalite, profession, adress_1,
      ville, tel1, tel2, whatapp, adress_2, contry, wire } = this.state.patient;

    //Confirmation
    confirmAlert({

      title: 'Confirmer pour soumettre',
      message: 'êtes-vous sûr de le faire.',
      buttons: [
        {
          label: 'Oui',
          onClick: () => {
            axios.put('/patients/' + this.props.match.params.id, {
               nom, prenom, sexe,
              groupe_sanguin, birth_date, birth_place, birth_rank, nationalite,
              profession, adress_1, ville, tel1, tel2, whatapp, adress_2, contry, wire
            })
              .then((result) => {
                this.props.history.push("/show/" + this.props.match.params.id)
                if (result.data != null) {
                  this.setState(this.initialState);
                  alert("Patient Modifié avec succés");
                }
              });


            alert('Click Oui')
          }
        },
        {
          label: 'Non',
          onClick: () => alert('Click Non')
        }
      ]
    });

  }

  //Méthode bouton Dossier Médicale

  ShowPatient = () => {
    return this.props.history.push(`/show/${this.state.patient.id}`);
  };





  render() {

    let { imagePreviewUrl } = this.state;
    let $imagePreview = null;
    return (
      <div className="container">

        <div className="content-wrapper">
          <div className="header bg-primary pb-6">
            <div className="container-fluid">
              <div className="header-body">
                <div className="row align-items-center py-4">
                  <div className="col-lg-6 col-7">
                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faList} /> Gestion des Patients</h6>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="container-fluid mt--6">
            <div className="row">
              <div className="col">
                <div className="card">
                  {/* Card header */}
                  <div className="card-header card-wite border-0">
                    <h3 className="mb-0"><FontAwesomeIcon icon={faEdit} /> Modifier Patient</h3>
                  </div>
                  <div className="card-body bg-white">
                    {/*formilair*/}

                    <Form onSubmit={this.onSubmit}>
                      <Container>
                        <Col className={"border border-white "}>
                          <Row>
                            <Col>
                              <Form.Group as={Col} controlId="formGridId">
                                <Form.Label>Nom</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="nom"
                                  onChange={this.onChange}
                                  value={this.state.patient.nom}
                                  className={"bg-white text-dark"}
                                  placeholder="Nom"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridId">
                                <Form.Label>Prenom</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="prenom"
                                  onChange={this.onChange}
                                  value={this.state.patient.prenom}
                                  className={"bg-white text-dark"}
                                  placeholder="Prenom"
                                />
                              </Form.Group>


                              <Form.Group as={Col} controlId="formGridSex">
                                <Form.Label>Sexe</Form.Label>
                                <Form.Control required as="select"
                                  custom onChange={this.onChange}
                                  name="sexe"
                                  value={this.state.patient.sexe}
                                  className={"bg-white text-dark"}>

                                  <option value=" ">Select</option>
                                  <option value="M">M</option>
                                  <option value="F">F</option>
                          )}
                        </Form.Control>
                              </Form.Group>
                            </Col>
                            <Col>
                              <Col>
                                <Col>


                                  {!$imagePreview && <img className={" border border-dark border border-white bg-white "} src={imagePreviewUrl
                                    || `data:image/png;base64,${this.state.patient.image.data}`} style={{ "width": "200px", "margin": "5Opx" }} />}

                                </Col>
                              </Col>
                              <Col>

                                <div>
                                  <span className="btn btn-primary btn-file"> Browse Image

                          <input onChange={this.handleFile} name="images" type="file"
                                      title="Upload Test Here"
                                      accept="image/gif, image/x-png, image/jpeg "
                                      data-toggle="tooltip" data-trigger="hover"
                                      className="form-control tooltips"
                                      style={{ "width": "165px", "margin": "10px" }} />
                                  </span>
                                </div>



                              </Col>
                            </Col>
                          </Row>
                        </Col>
                        <Col className={"border border-white "}>
                          <Row>
                            <Col>

                              <Form.Group as={Col} controlId="formGridSex">
                                <Form.Label>Groupe Sanguin</Form.Label>
                                <Form.Control required as="select"
                                  custom onChange={this.onChange}
                                  name="groupe_sanguin"
                                  value={this.state.patient.groupe_sanguin}
                                  className={"bg-white text-dark"}>

                                  <option value=" ">Select</option>
                                  <option value="O+">O+</option>
                                  <option value="O-">O-</option>
                                  <option value="A+">A+</option>
                                  <option value="A-">A-</option>
                                  <option value="B+">B+</option>
                                  <option value="B-">B-</option>
                                  <option value="AB+">AB+</option>
                                  <option value="AB-">AB-</option>
                      )}
                    </Form.Control>
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridId">
                                <Form.Label>Date de Naissance</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="date" name="birth_date"
                                  onChange={this.onChange}
                                  value={this.state.patient.birth_date}
                                  className={"bg-white text-dark"}
                                  placeholder="Date de Naissance"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridId">
                                <Form.Label>Lieu de Naissance</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="birth_place"
                                  onChange={this.onChange}
                                  value={this.state.patient.birth_place}
                                  className={"bg-white text-dark"}
                                  placeholder="Date de Naissance"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridId">
                                <Form.Label>Birth Rank</Form.Label>
                                <Form.Control
                                  type="number" name="birth_rank"
                                  onChange={this.onChange}
                                  value={this.state.patient.birth_rank}
                                  className={"bg-white text-dark"}
                                  placeholder="Birth Rank"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridNa">
                                <Form.Label>Nationalite</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="nationalite"
                                  onChange={this.onChange}
                                  value={this.state.patient.nationalite}
                                  className={"bg-white text-dark"}
                                  placeholder="Nationalite"

                                />

                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridPr">
                                <Form.Label>Profession</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="profession"
                                  onChange={this.onChange}
                                  value={this.state.patient.profession}
                                  className={"bg-white text-dark"}
                                  placeholder="Profession"
                                />
                              </Form.Group>
                              <Form.Group as={Col} controlId="formGridV">
                                <Form.Label>Ville</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="ville"
                                  onChange={this.onChange}
                                  value={this.state.patient.ville}
                                  className={"bg-white text-dark"}
                                  placeholder="Ville"
                                />
                              </Form.Group>


                            </Col>
                            <Col>

                              <Form.Group as={Col} controlId="formGridContry">
                                <Form.Label>Pays</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="text" name="contry"
                                  onChange={this.onChange}
                                  value={this.state.patient.contry}
                                  className={"bg-white text-dark"}
                                  placeholder="Pays"
                                />

                              </Form.Group>


                              <Form.Group as={Col} controlId="formGridAdress1">
                                <Form.Label>Adress 1</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="email" name="adress_1"
                                  onChange={this.onChange}
                                  value={this.state.patient.adress_1}
                                  className={"bg-white text-dark"}
                                  placeholder="nom@example.com"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridAdress2">
                                <Form.Label>Adress 2</Form.Label>
                                <Form.Control
                                  type="email" name="adress_2"
                                  onChange={this.onChange}
                                  value={this.state.patient.adress_2}
                                  className={"bg-white text-dark"}
                                  placeholder="nom@example.com"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridTelephone1">
                                <Form.Label>Telephone 1</Form.Label>
                                <Form.Control required autoComplete="off"
                                  type="number" name="tel1"
                                  onChange={this.onChange}
                                  className={"bg-white text-dark"}
                                  value={this.state.patient.tel1}
                                  placeholder="Telephone 1"

                                />

                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridTelephone2">
                                <Form.Label>Telephone 2</Form.Label>
                                <Form.Control
                                  type="number" name="tel2"
                                  onChange={this.onChange}
                                  value={this.state.patient.tel2}
                                  className={"bg-white text-dark"}
                                  placeholder="Telephone 2"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridWhatapp">
                                <Form.Label>Whatapp</Form.Label>
                                <Form.Control
                                  type="number" name="whatapp"
                                  onChange={this.onChange}
                                  value={this.state.patient.whatapp}
                                  className={"bg-white text-dark"}
                                  placeholder=" Whatapp"
                                />
                              </Form.Group>

                              <Form.Group as={Col} controlId="formGridWire">
                                <Form.Label>Wire</Form.Label>
                                <Form.Control
                                  type="text" name="wire"
                                  onChange={this.onChange}
                                  value={this.state.patient.wire}
                                  className={"bg-white text-dark"}
                                  placeholder=" Wire"
                                />
                              </Form.Group>

                            </Col>


                          </Row>
                        </Col>
                        {/* les  Boutons*/}
                        <Card.Footer style={{ "textAlign": "right" }}>
                          <button type="submit" class="btn btn-success" style={{ "width": "120px", "margin": "1px", "padding": "2px" }} ><FontAwesomeIcon icon={faEdit} /> Modifier</button>{' '}
                          <Button size="sm" variant="info" type="button" onClick={this.ShowPatient.bind()}>
                            <FontAwesomeIcon icon={faFolderOpen} /> Dossier du patient
                            </Button>
                        </Card.Footer>

                      </Container>
                    </Form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default EditPatient;
