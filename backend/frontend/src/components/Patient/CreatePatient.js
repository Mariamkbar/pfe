import React, { Component } from 'react';
import axios from "axios/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faSave, faPlusSquare, faUndo, faHospitalUser } from '@fortawesome/free-solid-svg-icons';
import { Card, Button, Col, Row, ProgressBar, Form } from 'react-bootstrap';
import "./imageCss.css";
import {confirmAlert} from "react-confirm-alert";

//Component Ajouter Patient
class CreatePatient extends Component {
    constructor() {
        super();
        //les attribus
        this.state = {
            nom: '',
            nomE: '',
            prenom: '',
            prenomE: '',
            sexe: '',
            sexeE: '',
            groupe_sanguin: '',
            groupe_sanguinE: '',
            birth_date: '',
            birth_dateE: '',
            birth_place: '',
            birth_placeE: '',
            birth_rankE: '',
            birth_rank: '',
            nationalite: '',
            nationaliteE: '',
            professionE: '',
            profession: '',
            ville: '',
            villeE: '',
            contry: '',
            contryE: '',
            adress_1: '',
            adress_1E: '',
            adress_2: '',
            adress_2E: '',
            tel1E: '',
            tel1: '',
            tel2E: '',
            tel2: '',
            whatappE: '',
            whatapp: '',
            wireE: '',
            wire: '',
            image: '',
            images: '',
            ErrorStatus: '',
            uploadPercentage: 0,

        };
        this.handleFile = this.handleFile.bind(this);
        this.onChange = this.onChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }



    //fuction qui gére l'image
    handleFile(e) {
        this.setState({ images: e.target.files[0] });
        this.setState({ image: '' });
        let reader = new FileReader();
        let images = e.target.files[0];
        reader.onloadend = () => {
            this.setState({ file: images, imagePreviewUrl: reader.result });
        }
        reader.readAsDataURL(images)

    }//Méthode Onchange inputs
    onChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });

    };


    //Méthode qui restart La formulair
    resetPatient = () => {
        this.setState(() => this.state);
    };
    //Méthode Pour la bouton liste des patients
    PatientListe = () => {
        return this.props.history.push("/listpas");
    };



    //Méthode post Data
    handleSubmit(e) {
        e.preventDefault();



        const formData = {
            nom: this.state.nom,
            prenom: this.state.prenom,
            sexe: this.state.sexe,
            groupe_sanguin: this.state.groupe_sanguin,
            birth_date: this.state.birth_date,
            birth_place: this.state.birth_place,
            birth_rank: this.state.birth_rank,
            nationalite: this.state.nationalite,
            profession: this.state.profession,
            ville: this.state.ville,
            contry: this.state.contry,
            adress_1: this.state.adress_1,
            adress_2: this.state.adress_2,
            tel1: this.state.tel1,
            tel2: this.state.tel2,
            whatapp: this.state.whatapp,
            wire: this.state.wire,


        };
        const { nom, prenom, sexe, groupe_sanguin, birth_date, birth_place, birth_rank, nationalite, profession, ville, contry, adress_1, adress_2, tel1, tel2, whatapp, wire } = this.state;
        let fd = new FormData();
        fd.append('imgs', this.state.images, this.state.images.name);
        fd.append('nom', nom);
        fd.append('prenom', prenom);
        fd.append('sexe', sexe);
        fd.append('groupe_sanguin', groupe_sanguin);
        fd.append('birth_date', birth_date);
        fd.append('birth_place', birth_place);
        fd.append('nationalite', nationalite);
        fd.append('ville', ville);
        fd.append('contry', contry);
        fd.append('adress_1', adress_1);
        fd.append('adress_2', adress_2);
        fd.append('tel1', tel1);
        fd.append('tel2', tel2);
        fd.append('profession', profession);
        fd.append('birth_rank', birth_rank);
        fd.append('whatapp', whatapp);
        fd.append('wire', wire);


        console.log(fd);
        const options = {
            onUploadProgress: (progressEvent) => {
                const { loaded, total } = progressEvent;
                let percent = Math.floor(((loaded / 1000) * 100) / (total / 1000))
                console.log(`${loaded}kb of ${total}kb | ${percent}%`);
                this.setState({ uploadPercentage: percent })

            }
        }

        //Confirmation
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
        axios.post('http://localhost:8080/patients', fd, options)


            .then((response) => {
                console.log(response)
                this.setState({ image: response.data.url, uploadPercentage: 100 }, () => {
                    setTimeout(() => {
                        this.setState({ uploadPercentage: 0 })
                    }, 1000);

                })

                if (response.data === 'Success') {
                    alert('Patient enregisté avec succés..!!');
                }

                this.props.history.push("/listpas")
            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });
    }


    render() {
        let { imagePreviewUrl } = this.state;
        let $imagePreview = null;
        const { uploadPercentage } = this.state;
        return (
            <div className="container">

                <div className="content-wrapper">
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0">
                                            <FontAwesomeIcon icon={faHospitalUser} /> Gestion des Patients</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header card-wite border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Patient
                                        </h3>
                                    </div>
                                    <div className="card-body bg-white">

                                        {/*formilair*/}

                                        <Form stencType="multipart/form-data" onSubmit={this.handleSubmit} onReset={this.resetPatient}
                                            className="form-horizontal form-bordered" method="post">
                                            <Col className={"border border-white "}>
                                                <Row>
                                                    <Col>

                                                        <Form.Group as={Col} controlId="formGridNom">
                                                            <Form.Label>Nom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="nom"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Nom"
                                                                title="Select nomE nom "
                                                                data-toggle="tooltip" data-trigger="hover"
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.nomE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridPrenom">
                                                            <Form.Label>Prenom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="prenom"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Prenom"
                                                                title="Select prenomE prenom "
                                                                data-toggle="tooltip" data-trigger="hover"

                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.prenomE}
                                                            </div>
                                                        </Form.Group>


                                                        <Form.Group as={Col} controlId="formGridSex">
                                                            <Form.Label>Sexe</Form.Label>
                                                            <Form.Control required as="select"
                                                                custom onChange={this.onChange}
                                                                name="sexe"
                                                                title="Select sexe "
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                className={"bg-white text-dark"}>

                                                                <option value=" ">Select</option>
                                                                <option value="M">M</option>
                                                                <option value="F">F</option>

                                                            </Form.Control>
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.sexeE}
                                                            </div>
                                                        </Form.Group>
                                                    </Col>

                                                    <Col>
                                                        <Col>
                                                            <Col>

                                                                {!$imagePreview && <img class="rounded-circle"
                                                                    className={" border border-dark border border-white bg-white "}
                                                                    src={imagePreviewUrl} style={{ "width": "200px", "margin": "5Opx" }}
                                                                />}
                                                            </Col>
                                                        </Col>
                                                        <Col>
                                                            <div>
                                                                <span className="btn btn-primary btn-file"> Browse Image

                                                                <input onChange={this.handleFile} name="images" type="file" accept="image/gif, image/x-png, image/jpeg "
                                                                        required autoComplete="off"
                                                                        title="Upload Test Here"
                                                                        data-toggle="tooltip" data-trigger="hover"
                                                                        className="form-control tooltips"
                                                                        style={{ "width": "165px", "margin": "10px" }} />
                                                                </span>
                                                            </div>

                                                        </Col>

                                                    </Col>
                                                </Row>
                                            </Col>
                                            <Col className={"border border-white "}>
                                                <Row>
                                                    <Col>
                                                        <Form.Group as={Col} controlId="formGridGS">
                                                            <Form.Label>Groupe Sanguin</Form.Label>
                                                            <Form.Control required as="select"
                                                                custom onChange={this.onChange}
                                                                name="groupe_sanguin"
                                                                title="Select groupe sanguin "
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                className={"bg-white text-dark"}>

                                                                <option value=" ">Select</option>
                                                                <option value="O+">O+</option>
                                                                <option value="O-">O-</option>
                                                                <option value="A+">A+</option>
                                                                <option value="A-">A-</option>
                                                                <option value="B+">B+</option>
                                                                <option value="B-">B-</option>
                                                                <option value="AB+">AB+</option>
                                                                <option value="AB-">AB-</option>

                                                            </Form.Control>
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.groupe_sanguinE}
                                                            </div>

                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridDate">
                                                            <Form.Label>Date Naissance</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="date" name="birth_date"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Date"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.birth_dateE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridLieu">
                                                            <Form.Label>Lieu Naissance</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="birth_place"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Lieu"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.birth_placeE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridBr">
                                                            <Form.Label>Birth Rank</Form.Label>
                                                            <Form.Control
                                                                type="number" name="birth_rank"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Birth Rank"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select birth_rank "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.birth_rankE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridNa">
                                                            <Form.Label>Nationalite</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="nationalite"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Nationalite"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select nationalite "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.nationaliteE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridPr">
                                                            <Form.Label>Profession</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="profession"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Profession"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select profession "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.professionE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridV">
                                                            <Form.Label>Ville</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="ville"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Ville"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select ville "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.villeE}
                                                            </div>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col>

                                                        <Form.Group as={Col} controlId="formGridContry">
                                                            <Form.Label>Pays</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text" name="contry"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Pays"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select contry "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.contryE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridAdress1">
                                                            <Form.Label>Adress 1</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="email" name="adress_1"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="nom@example.com"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select adress_1 "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.adress_1E}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridAdress2">
                                                            <Form.Label>Adress 2</Form.Label>
                                                            <Form.Control
                                                                type="email" name="adress_2"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="nom@example.com"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select adress_2 "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.adress_2E}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridTelephone1">
                                                            <Form.Label>Telephone 1</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="number" name="tel1"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Telephone 1"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select tel1 "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.tel1E}
                                                            </div>
                                                        </Form.Group>


                                                        <Form.Group as={Col} controlId="formGridTelephone2">
                                                            <Form.Label>Telephone 2</Form.Label>
                                                            <Form.Control
                                                                type="number" name="tel2"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder="Telephone 2"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select tel2 "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.tel2E}
                                                            </div>
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridWhatapp">
                                                            <Form.Label>Whatapp</Form.Label>
                                                            <Form.Control
                                                                type="number" name="whatapp"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder=" Whatapp"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select whatapp "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.whatappE}
                                                            </div>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridWire">
                                                            <Form.Label>Wire</Form.Label>
                                                            <Form.Control
                                                                type="text" name="wire"
                                                                onChange={this.onChange}
                                                                className={"bg-white text-dark"}
                                                                placeholder=" Wire"
                                                                data-toggle="tooltip" data-trigger="hover"
                                                                title="Select wire "
                                                            />
                                                            <div style={{ fontSize: 12, color: "red " }}>
                                                                {this.state.wireE}
                                                            </div>
                                                        </Form.Group>

                                                    </Col>

                                                </Row>
                                                {uploadPercentage > 0 && <ProgressBar animated style={{ "height": "30px" }} now={uploadPercentage} active label={`${uploadPercentage}%`} />}

                                            </Col>
                                            {/* les  Boutons*/}
                                            <Card.Footer style={{ "textAlign": "right" }}>

                                                {'       '}

                                                <button type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}>
                                                    <FontAwesomeIcon icon={faSave} /> Enregister</button>{' '}
                                                <Button size="sm" variant="info" type="reset" onClick={this.resetPatient.bind()}>
                                                    <FontAwesomeIcon icon={faUndo} /> Refreche
                                                </Button>{' '}
                                                <Button size="sm" variant="info" type="button" onClick={this.PatientListe.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Patients
                                                </Button>

                                            </Card.Footer>
                                        </Form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CreatePatient;