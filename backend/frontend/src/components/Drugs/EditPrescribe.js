import React, { useEffect, useRef, useState } from "react";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faEdit,
    faPlusSquare,
    faDisease,
    faList,
    faStop,
    faHourglassStart,
    faUndo
} from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';



import http from "../../http-common";
import { getDrug } from "./Requests";
import { confirmAlert } from "react-confirm-alert";
import Select from "react-select";
import {Label} from "./styles";


const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(8, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    dosage: Yup.number()
        .min(20, "C'est trop court")
        .max(2000, "C'est trop long")
        .required("Veuillez saisir un dosage"),
    drug: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir code médicament"),
    })
});
export default function EditPrescribe(props) {
    const [duration, setDuration] = useState(0);
    const [delay, setDelay] = useState(1000);

    useInterval(() => setDuration((c) => c + 1), delay);

    useEffect(() => {
        if (delay == null) alert(`Duration ${duration}`);
    }, [duration, delay]);

    function useInterval(callback, delay) {
        const savedCallback = useRef();

        useEffect(() => {
            savedCallback.current = callback;
        });

        useEffect(() => {
            let id;
            if (delay) {
                id = setInterval(savedCallback.current, delay);
            }
            return () => clearInterval(id);
        }, [delay]);
    }

    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const [errorRequest, setErrorRequest] = useState(false);
    const [drugList, setDrug] = useState([]);
    const [selectIndex, setSelectIndex] = useState(0);
    useEffect(() => {
        async function fetchInitialData() {
            const response = await getDrug();
            response.errors ? setErrorRequest(true) : setDrug(response);
        }

        fetchInitialData();
    }, []);

    const initialStates = {
        nom: "", dosage: "", duration: 0, drugs: [{}]
    }


    const [currentStates, setCurrentStates] = useState(initialStates);


    const getById = id => {
        const get = id => {
            return http.get(`/prescribes/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data,response.data.drugs.map((e) => ({ label: e.nom , value: e })));
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };

    const onChange = selectedOptions =>{
        let drugs;
        if(drugs){
     setCurrentStates(selectedOptions)}} ;


    const updateOrdonnace = () => {
        console.log(options);

        let drugs;
        alert(JSON.stringify(options, null, 5))
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.put(`/prescribes/${id}`, data);
                        };
                        update(currentStates.id, { nom: currentStates.nom,
                            dosage: currentStates.dosage, drugs: currentStates.drugs.map(t => t.value),

                            duration: duration })
                            .then(response => {
                                props.history.push("/listpr");
                                alert("Ordonnance modifié avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const Liste = () => {
        return props.history.push("/listpr");
    };
    const options= currentStates.drugs.map((e) => ({ label: e.nom , value: e }));
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Médicaments</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Ordonnance
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <div style={{ textAlign: 'center' }}>
                                        <h3>Duration</h3>
                                        <h2>{duration}</h2>
                                        <button className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }} onClick={() => setDuration(0)}>
                                            <FontAwesomeIcon icon={faUndo} /> Reset
                                        </button>
                                        <button
                                            className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                            onClick={() => setDelay((delay) => (delay ? null : 1000))}
                                        >
                                            <FontAwesomeIcon icon={delay ? faStop : faHourglassStart} /> {delay ? "Pause" : "Start"}
                                        </button>


                                    </div>
                                    {currentStates && (
                                        <div className="edit-form">

                                            <form>
                                                      Médicament
                                                        <Select
                                                            name="drugs"
                                                            closeMenuOnSelect={false}
                                                            options={drugList.map((e) => ({ label: e.nom , value: e }))}
                                                            isMulti
                                                            values={options}
                                                            onChange={onChange}
                                                        />

                                                    <Form.Group as={Col} controlId="formGridDosage">
                                                        <Form.Label>Dosage</Form.Label>
                                                        <Form.Control required autoComplete="off"
                                                            type="text"
                                                            id="dosage"
                                                            name="dosage"
                                                            placeholder="Dosage"
                                                            value={currentStates.dosage}
                                                            className={"bg-white text-dark"}
                                                            onChange={handleInputChange}
                                                        />
                                                    </Form.Group>

                                                    <Form.Group as={Col} controlId="formGridNom">
                                                        <Form.Label>Nom</Form.Label>
                                                        <Form.Control required autoComplete="off"
                                                                      type="text"
                                                                      id="nom"
                                                                      name="nom"
                                                                      placeholder="Nom"
                                                                      value={currentStates.nom}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        />
                                                    </Form.Group>
                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateOrdonnace}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Ordonnances
                                                </Button>
                                            </Card.Footer>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
