import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faList, faTrash, faPills, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { Button, ButtonGroup } from "react-bootstrap";

import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function DrugList(props) {

    const url = '/drugs'
    const [data, setData] = useState([])
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(3);
    const [pageCount, setPageCount] = useState(0)
    const [orgtableData, setOrgtableData] = useState([]);

    const loadMoreData = () => {
        const data = orgtableData;

        const slice = data.slice(
            offset,
            offset + perPage
        );

        setPageCount(Math.ceil(data.length / perPage))
        setData(slice)
    }

    const getData = async () => {
        axios.get(`/drugs`).then((res) => {
            setData(res.data);
            console.log(data);
            const data = res.data;

            const slice = data.slice(offset, offset + perPage)

            setPageCount(Math.ceil(data.length / perPage))
            setOrgtableData(res.data)
            setData(slice)
        });

    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setPageCount(selectedPage);
        setOffset(offset, () => {
            loadMoreData();
        });



    };


    useEffect(() => {
        retrieveDrug();
        getData()

    }, [offset]

    )

    const retrieveDrug = () => {
        axios.get(url).then(json => setData(json.data))

    };

    const refreshList = () => {
        retrieveDrug();

    };

    const deleteDrug = (id) => {
        console.log(id);
        axios.delete(`/drugs/${id}`)
            .then((result) => {
                if (result.data != null) {
                }
                refreshList();
            });

    };

    const CreateDrog = () => {
        return props.history.push("/adddrugs");
    };




    const renderTable = () => {
        return data.map(con => {
            return (
                <tr>
                    <td>{con.nom}</td>
                    <td>{con.brand}</td>
                    <td>{con.indication}</td>
                    <td>{con.contraindication}</td>
                    <td>
                        <ButtonGroup>
                            <Link to={`/editdrog/${con.id}`} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>&nbsp;
                            {/* <Button size="sm" variant="outline-danger" onClick={() => { if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?')) deleteDrug(con.id) }}><FontAwesomeIcon icon={faTrash} /></Button>*/}
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }
    return (
        <div>
            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">

                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Médicaments
                                            <Button size="sm" variant="info" type="button"
                                                style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }}
                                                onClick={CreateDrog.bind()}>
                                                <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Médicament
                                            </Button>
                                        </h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        <table class="table table-stripe"  >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort">Nom</th>
                                                    <th scope="col" className="sort">Brand </th>
                                                    <th scope="col" className="sort">Indication </th>
                                                    <th scope="col" className="sort">Contraindication </th>
                                                    <th scope="col" className="sort">Action </th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">{renderTable()}</tbody>
                                        </table>
                                        <ReactPaginate
                                            previousLabel={"prev"}
                                            nextLabel={"next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"} />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
