import React, { useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare, faPills, faSave, faUndo, faList } from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';
import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";
import { confirmAlert } from "react-confirm-alert";


const formSchema = Yup.object().shape({

    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un nom"),
    brand: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un brand"),
    indication: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un indication"),
    contraindication: Yup.string()
        .min(2, "C'est trop court")
        .max(500, "C'est trop long")
        .required("Veuillez saisir un contraindication"),
});
export default function CreateDrug(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };
    const handleOnSubmit = (values, actions) => {
        console.log('Form Data',values);
        alert(JSON.stringify(values, null, 2))
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/drugs", values
                        )
                            .then(response => {
                                props.history.push("/listdrugs");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                alert("Drug enregisté avec succés");
                                console.log(response.data);
                                console.log('Form Data',values);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const [initialValues, setInitialValues] = useState(
        {
             nom: "", brand: "", indication: "", contraindication: ""
        }
    );
    const Liste = () => {
        return props.history.push("/listdrugs");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faPills} /> Gestion des Médicaments</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Médicament
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid
                                        }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>


                                                        <Label htmlFor="nom">
                                                            Nom
                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}


                                                        <Label htmlFor="brand">
                                                            Brand
                                            <Input
                                                                type="text"
                                                                name="brand"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Brand"
                                                                valid={touched.brand && !errors.brand}
                                                                error={touched.brand && errors.brand}
                                                            />
                                                        </Label>
                                                        {errors.brand && touched.brand && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.brand}
                                                            </StyledInlineErrorMessage>
                                                        )}
                                                    </Col>
                                                    <Col>

                                                        <Label htmlFor="indication">
                                                            Indication
                                            <Input
                                                                type="text"
                                                                name="indication"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Indication"
                                                                valid={touched.indication && !errors.indication}
                                                                error={touched.indication && errors.indication}
                                                            />
                                                        </Label>
                                                        {errors.indication && touched.indication && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.indication}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="contraindication">
                                                            Contraindication
                                                            <Input
                                                                type="text"
                                                                name="contraindication"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Contraindication"
                                                                component="textarea"
                                                                valid={touched.contraindication && !errors.contraindication}
                                                                error={touched.contraindication && errors.contraindication}
                                                            />
                                                        </Label>
                                                        {errors.contraindication && touched.contraindication && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.contraindication}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                    </Col>



                                                </Row>



                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Médicaments
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
