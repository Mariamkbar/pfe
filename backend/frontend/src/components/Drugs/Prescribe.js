import React, { useEffect, useRef, useState } from "react";
import axios from "axios";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faPlusSquare,
    faPills,
    faSave,
    faUndo,
    faList,
    faStop,
    faHourglassStart
} from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';

import { getDrug } from './Requests';
import ReactMultiSelectCheckboxes from 'react-multiselect-checkboxes';

import {
    Label,
    Input,
    StyledInlineErrorMessage
} from "./styles";
import { confirmAlert } from "react-confirm-alert";
import Select from "react-select"

const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un nom"),
    dosage: Yup.string()
        .min(2, "C'est trop court")
        .max(500, "C'est trop long")
        .required("Veuillez saisir un dosage"),
    drugs: Yup.array()
        .min(1, 'au moin 1 médicament')
        .max(4, 'au plus 4 médicament')
        .of(
            Yup.object().shape({
                label: Yup.string().required(),
                value: Yup.string().required(),
            })
        ),


});
export default function CreatePrescribe(props) {


    const [duration, setDuration] = useState(0);
    const [delay, setDelay] = useState(1000);
    const durationRef = useRef();

    const [errorRequest, setErrorRequest] = useState(false);
    const [drug, setDrug] = useState([]);
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const options = [
        { label: 'Thing 1', value: 1},
        { label: 'Thing 2', value: 2},
    ];

    useInterval(() => setDuration((c) => c + 1), delay);

    useEffect(() => {
        if (delay == null) alert(`Duration ${duration}`);
    }, [duration, delay]);

    function useInterval(callback, delay) {
        const savedCallback = useRef();

        useEffect(() => {
            savedCallback.current = callback;
        });

        useEffect(() => {
            let id;
            if (delay) {
                id = setInterval(savedCallback.current, delay);
            }
            return () => clearInterval(id);
        }, [delay]);
    }

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getDrug();
            response.errors ? setErrorRequest(true) : setDrug(response);
        }

        fetchInitialData();
    }, []);
    const handleOnSubmit = (values, actions) => {
        console.log('Form Data',values);
        alert(JSON.stringify(values, null, 6))
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {

                          axios.post("/prescribes", {
                            id: values.id,
                            dosage: values.dosage,
                              drugs: values.drugs.map(t => t.value),
                            duration: duration,
                                  nom: values.nom
                        }
                        )
                            .then(response => {
                                props.history.push("/listpr");
                                actions.setSubmitting(false);
                                console.log(response.data);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                if (delay == null) alert(`Duration ${duration} `);
                                alert("Prescribe enregisté avec succés");
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });
    };
    const [initialValues, setInitialValues] = useState(
        {
            nom: "", dosage: "", duration: "", drug: { nom: "" },drugs: [{}]
        }
    );
    const Liste = () => {
        return props.history.push("/listpr");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };





    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faPills} />  Donner des Ordonnances </h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Ordonnance
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={{
                                            nom: "", dosage: "", duration: "",drugs: [{}]
                                        }}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid,
                                            setFieldValue,
                                              setFieldTouched,
                                          }) => (
                                            <Form id="fs-frm" noValidate>

                                                <div style={{ textAlign: 'center' }}>
                                                    <h3>Duration</h3>
                                                    <h2>{duration}</h2>
                                                    <button className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }} onClick={() => setDuration(0)}>
                                                        <FontAwesomeIcon icon={faUndo} /> Reset
                                        </button>
                                                    <button
                                                        className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        onClick={() => setDelay((delay) => (delay ? null : 1000))}
                                                    >
                                                        <FontAwesomeIcon icon={delay ? faStop : faHourglassStart} />  {delay ? "Pause" : "Start"}
                                                    </button>


                                                </div>
                                                <Row>
                                                     <Label htmlFor="drugs">
                                                        Médicament
                                                        <Select
                                                            name="drugs"
                                                            closeMenuOnSelect={false}
                                                            options={drug.map((e) => ({ label: e.nom , value: e }))}
                                                            isMulti
                                                            values={values.drugs}
                                                            onChange={(
                                                                selectedValue
                                                            ) => setFieldValue('drugs', selectedValue)}
                                                            valid={touched.drugs && !errors.drugs}
                                                            error={touched.drugs && errors.drugs}
                                                            reauired

                                                        />
                                                    </Label>





                                                        <Label htmlFor="nom">
                                                            Nom
                                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}


                                                    <Label htmlFor="dosage">
                                                        Dosage
                                                        <Input
                                                            type="text"
                                                            name="dosage"
                                                            autoCorrect="off"
                                                            autoComplete="name"
                                                            placeholder="Dosage"
                                                            valid={touched.dosage && !errors.dosage}
                                                            error={touched.dosage && errors.dosage}
                                                        />
                                                    </Label>
                                                    {errors.dosage && touched.dosage && (
                                                        <StyledInlineErrorMessage>
                                                            {errors.dosage}
                                                        </StyledInlineErrorMessage>
                                                    )}


                                                </Row>




                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Ordonnances
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
