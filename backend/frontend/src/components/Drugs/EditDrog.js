import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlusSquare, faDisease, faList } from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';



import http from "../../http-common";
import { confirmAlert } from "react-confirm-alert";
import organeService from "../../services/organeService";



const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(8, "C'est trop long")
        .required("Veuillez saisir un nom"),
    brand: Yup.string()
        .min(2, "C'est trop court")
        .max(8, "C'est trop long")
        .required("Veuillez saisir un brand"),
    indication: Yup.string()
        .min(2, "C'est trop court")
        .max(8, "C'est trop long")
        .required("Veuillez saisir un indication"),
    contraindication: Yup.string()
        .min(2, "C'est trop court")
        .max(40, "C'est trop long")
        .required("Veuillez saisir un contraindication"),
});
export default function EditDrog(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };


    const initialStates = {
         nom: "", brand: "", indication: "", contraindication: ""
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        const get = id => {
            return http.get(`/drugs/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };







    const updateDrog = () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.put(`/drugs/${id}`, data);
                        };
                        update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/listdrugs");
                                alert("Médicament modifié avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });


    };
    const Liste = () => {
        return props.history.push("/listdrugs");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Médicaments</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Médicament
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Row>
                                                    <Col>

                                                        <Form.Group as={Col} controlId="formGridNom">
                                                            <Form.Label>Nom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="nom"
                                                                name="nom"
                                                                placeholder="Nom"
                                                                value={currentStates.nom}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridBrand">
                                                            <Form.Label>Brand</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="brand"
                                                                name="brand"
                                                                placeholder="Brand"
                                                                value={currentStates.brand}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                    </Col>
                                                    <Col>
                                                        <Form.Group as={Col} controlId="formGridIndication">
                                                            <Form.Label>Indication</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="indication"
                                                                name="indication"
                                                                placeholder="Indication"
                                                                value={currentStates.indication}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridContraindication">
                                                            <Form.Label>Contraindication</Form.Label>
                                                            <Form.Control required as="textarea"
                                                                type="text"
                                                                id="contraindication"
                                                                name="contraindication"
                                                                placeholder="Contraindication"
                                                                value={currentStates.contraindication}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>


                                                    </Col>


                                                </Row>


                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateDrog}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Médicaments
                                                </Button>
                                            </Card.Footer>
                                            <p>{message}</p>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
