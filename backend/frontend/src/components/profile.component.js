import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import AuthService from "../services/auth.service";
export default class Profile extends Component {
  constructor(props) {
    super(props);

    this.state = {
      redirect: null,
      userReady: false,
      currentUser: { username: "" }
    };
  }

  componentDidMount() {
    const currentUser = AuthService.getCurrentUser();

    if (!currentUser) this.setState({ redirect: "/home" });
    this.setState({ currentUser: currentUser, userReady: true })
  }

  render() {
    if (this.state.redirect) {
      return <Redirect to={this.state.redirect} />
    }

    const { currentUser } = this.state;

    return (
        <div>
          <div className="content-wrapper">
            <div>
              <div className="header bg-primary pb-6">
                <div className="container-fluid">
                  <div className="header-body">
                    <div className="row align-items-center py-4">
                      <div className="col-lg-6 col-7">
                        <h6 className="h2 text-white d-inline-block mb-0"><header className="jumbotron">
                          <h3>
                            <img
                                src={require('./avatar_2x.png')}
                                alt="profile-img"
                                className="profile-img-card"
                            /> <strong>{currentUser.username}</strong> Profile
                          </h3>
                        </header></h6>


                      </div>
                    </div>
                  </div>
                </div>
              </div>
              {/* Page content */}
              <div className="container-fluid mt--6">
                <div className="row">
                  <div className="col">
                    <div className="card">
                      {/* Card header */}

                      {/* Light table */}
                      <div className="container">
                        {(this.state.userReady) ?
                            <div>

                              <ul className="list-group list-group-unbordered mb-3">
                                <li className="list-group-item">
                                  <b><strong>Id</strong></b> <a className="float-right">{currentUser.id}</a>
                                </li>
                                <li className="list-group-item">
                                  <b>Email</b> <a className="float-right">{currentUser.email}</a>
                                </li>
                                <li className="list-group-item">
                                  <b>Authorities</b> <a className="float-right"><ul>
                                  {currentUser.roles &&
                                  currentUser.roles.map((role, index) => <li key={index}>{role}</li>)}
                                </ul></a>
                                </li>
                                <li className="list-group-item">
                                  <b><strong>Token</strong></b> <a className="float-right">{currentUser.accessToken.substring(0, 20)} ...{" "}
                                  {currentUser.accessToken.substr(currentUser.accessToken.length - 20)}</a>
                                </li>
                              </ul>

                            </div>: null}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
    );
  }
}
