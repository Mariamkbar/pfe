import React from 'react'

export default function Footer() {
    return (
        <div>
            <footer className="main-footer">
                <strong>Copyright © 2020-2021 <a href="http://adminlte.io">Binor</a>.</strong>
                All rights reserved.
                <div className="float-right d-none d-sm-inline-block">
                    <b>Dockfiles</b>
                </div>
            </footer>
        </div>

    )
}
