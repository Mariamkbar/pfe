import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { ButtonGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faList, faEdit, faTrash, faNotesMedical, faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import "./styles.css";
import ReactPaginate from "react-paginate";

export default function ConsultationList(props) {

    const url = '/consultations'
    const [data, setData] = useState([])
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(3);
    const [pageCount, setPageCount] = useState(0)
    const [orgtableData, setOrgtableData] = useState([]);

    const loadMoreData = () => {
        const data = orgtableData;

        const slice = data.slice(
            offset,
            offset + perPage
        );

        setPageCount(Math.ceil(data.length / perPage))
        setData(slice)
    }

    const getData = async () => {
        axios.get(`/consultations`).then((res) => {
            setData(res.data);
            console.log(data);
            const data = res.data;

            const slice = data.slice(offset, offset + perPage)

            setPageCount(Math.ceil(data.length / perPage))
            setOrgtableData(res.data)
            setData(slice)
        });

    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setPageCount(selectedPage);
        setOffset(offset, () => {
            loadMoreData();
        });



    };


    useEffect(() => {

        retrieveConsultation();
        getData();

    }, [offset]

    )



    const retrieveConsultation = () => {
        axios.get(url).then(json => setData(json.data))

    };

    const refreshList = () => {
        retrieveConsultation();

    };

    const deleteCons = (id) => {
        console.log(id);
        axios.delete(`/consultations/${id}`)
            .then((result) => {
                if (result.data != null) {
                    alert('Consultation été suprimer!!');
                }
                refreshList();
            });

    };

    const CreateConsultation = () => {
        return props.history.push("/addcons");
    };



    const renderTable = () => {

        return data?.map(con => {
            return (
                <tr>
                    <td>{con.titre}</td>
                    <td>{con.observation}</td>
                    <td>{con.date}</td>
                    <td>{con.duration}</td>
                    <td>{con.cost}</td>
                    <td>{con.diagnostic}</td>
                    <td>{con.status}</td>
                    <td>{con.patient.nom}</td>
                    <td>{con.medecin.nom}</td>
                    <td>
                        <ButtonGroup>
                            <Link to={`editcon/${con.id}`} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>{' '}
                            {/*   <Button size="sm" variant="outline-danger" onClick={() => { if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?')) deleteCons(con.id) }}><FontAwesomeIcon icon={faTrash} /></Button>*/}
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }
    return (
        <div>

            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faNotesMedical} /> Gestion des Consultations</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Consultations <Button size="sm" variant="info" type="button" style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }} onClick={CreateConsultation.bind()}>
                                            <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Consultation
                                        </Button>
                                        </h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        <table class="table table-stripe"  >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort">Titre</th>
                                                    <th scope="col" className="sort">Observation</th>
                                                    <th scope="col" className="sort">Date</th>
                                                    <th scope="col" className="sort">Duration</th>
                                                    <th scope="col" className="sort">Cost</th>
                                                    <th scope="col" className="sort">Diagnostic</th>
                                                    <th scope="col" className="sort">Status</th>
                                                    <th scope="col" className="sort">Patient </th>
                                                    <th scope="col" className="sort">Medecin </th>
                                                    <th scope="col" className="sort">Action</th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">{renderTable()}</tbody>
                                        </table>


                                    </div>

                                    <ReactPaginate
                                        previousLabel={"prev"}
                                        nextLabel={"next"}
                                        breakLabel={"..."}
                                        breakClassName={"break-me"}
                                        pageCount={pageCount}
                                        marginPagesDisplayed={2}
                                        pageRangeDisplayed={5}
                                        onPageChange={handlePageClick}
                                        containerClassName={"pagination"}
                                        subContainerClassName={"pages pagination"}
                                        activeClassName={"active"} />


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};