import React, { useState, useRef, useEffect, useCallback } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faSave,
    faPlusSquare,
    faNotesMedical,
    faUndo,
    faList,
    faStop,
    faHourglassStart
} from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';
import { getPatients, getMedecin, getPrescribe, getDiagnostics, getResult } from './Requests';
import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./style";
import { confirmAlert } from "react-confirm-alert";



const formSchema = Yup.object().shape({
    titre: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un titre "),
    observation: Yup.string()
        .min(2, "C'est trop court")
        .max(500, "C'est trop long")
        .required("Veuillez saisir une observation "),
    date: Yup.date().required("Veuillez saisir un date"),
    cost: Yup.number()
        .min(10, "C'est trop court")
        .max(200000, "C'est trop long")
        .required("Veuillez saisir un Coût"),
    diagnostic: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un diagnostique"),
    status: Yup.string()
        .required("Veuillez saisir un statut"),
    patient: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un patient')
    }),
    medecin: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un medecin')
    })
    ,
    prescribe: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir une ordonnance')
    })
    ,
    diagnostics: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un diagnostique ')
    })
    ,
    conducted_physical_exam: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un resultat d'examen")
    })
});


export default function Consultation(props) {
    /* Server State Handling */

    const [duration, setDuration] = useState(0);
    const [delay, setDelay] = useState(1000);
    const durationRef = useRef();
    const [patient, setPatient] = useState([]);
    const [medecin, setMedecin] = useState([]);
    const [prescribe, setPrescribe] = useState([]);
    const [diagnostic, setDiagnostic] = useState([]);
    const [result, setResult] = useState([]);
    const [errorRequest, setErrorRequest] = useState(false);
    useEffect(() => {
        durationRef.current = duration; // Write it to the ref
    });

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getPatients();
            response.errors ? setErrorRequest(true) : setPatient(response);
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getMedecin();
            response.errors ? setErrorRequest(true) : setMedecin(response);
        }

        fetchInitialData();
    }, []);
    useEffect(() => {
        async function fetchInitialData() {
            const response = await getPrescribe();
            response.errors ? setErrorRequest(true) : setPrescribe(response);
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getDiagnostics();
            response.errors ? setErrorRequest(true) : setDiagnostic(response);
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getResult();
            response.errors ? setErrorRequest(true) : setResult(response);
        }

        fetchInitialData();
    }, []);



    useInterval(() => setDuration((c) => c + 1), delay);

    useEffect(() => {
        if (delay == null) alert(`Duration ${duration}`);
    }, [duration, delay]);

    function useInterval(callback, delay) {
        const savedCallback = useRef();

        useEffect(() => {
            savedCallback.current = callback;
        });

        useEffect(() => {
            let id;
            if (delay) {
                id = setInterval(savedCallback.current, delay);
            }
            return () => clearInterval(id);
        }, [delay]);
    }

    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };
    const currentDuration = durationRef.current;
    const [initialValues, setInitialValues] = useState(
        {
            duration: 0,
            patient: { id: "" },
            medecin: { id: "" },
            prescribe: { id: "" },
            diagnostics: { id: "" },
            conducted_physical_exam: { id: "" },
            titre: "",
            observation: "",
            date: "",
            cost: "",
            diagnostic: "",
            status: ""
        }
    );

    const handleOnSubmit = (values, actions) => {
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/consultations", {
                            titre: values.titre,
                            observation: values.observation,
                            date: values.date,
                            cost: values.cost,
                            diagnostic: values.diagnostic,
                            status: values.status,
                            duration: duration,
                            patient: values.patient,
                            medecin: values.medecin,
                            prescribe: values.prescribe,
                            diagnostics: values.diagnostics,
                            conducted_physical_exam: values.conducted_physical_exam

                        }
                        )
                            .then(response => {
                                props.history.push("/listcons");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                if (delay == null) alert(`Duration ${duration} `);
                                alert(currentDuration)
                                alert("Consultation enregisté  avec succés");
                                console.log(response.data);

                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };


    const ConsultationsListe = () => {
        return props.history.push("/listcons");
    };
    const resetConsultation = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container ">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faNotesMedical} /> Gestion des Consultations</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Consultation
                                    </h3>
                                </div>


                                <div className="card-body bg-white">
                                    <Formik
                                        enableReinitialize
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            errors,
                                            touched,
                                            values,
                                            setFieldValue
                                        }) => (

                                            <Form id="fs-frm" noValidate >

                                                <div style={{ textAlign: 'center' }}>
                                                    <h3>Duration</h3>
                                                    <h2>{duration}</h2>
                                                    <button className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }} onClick={() => setDuration(0)}>
                                                        <FontAwesomeIcon icon={faUndo} />Reset
                            </button>
                                                    <button
                                                        className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        onClick={() => setDelay((delay) => (delay ? null : 1000))}
                                                    >
                                                        <FontAwesomeIcon icon={delay ? faStop : faHourglassStart} /> {delay ? "Pause" : "Start"}
                                                    </button>


                                                </div>
                                                <Row>

                                                    <Col>

                                                        <Label htmlFor="titre">
                                                            Titre
                                <Input
                                                                type="text"
                                                                name="titre"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Titre"
                                                                valid={touched.titre && !errors.titre}
                                                                error={touched.titre && errors.titre}
                                                            />
                                                        </Label>
                                                        <Label htmlFor="status">
                                                            Statut
                                <Input id="status" name="status" component="select"
                                                                valid={touched.status && !errors.status}
                                                                error={touched.status && errors.status}>
                                                                <option value=" " >Choisir un Statut </option>
                                                                <option value="ST1">ST1</option>
                                                                <option value="ST2">ST2</option>
                                                            </Input>

                                                        </Label>

                                                        <Label htmlFor="date">
                                                            Date
                                <Input
                                                                type="date"
                                                                name="date"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Date"
                                                                valid={touched.date && !errors.date}
                                                                error={touched.date && errors.date}
                                                            />
                                                        </Label>

                                                        <Label htmlFor="cost">
                                                            Coût
                                <Input
                                                                type="number"
                                                                name="cost"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Coût"
                                                                valid={touched.cost && !errors.cost}
                                                                error={touched.cost && errors.cost}
                                                            />
                                                        </Label>


                                                        <Label htmlFor="patient">
                                                            Patient
                                <Input id="patient.id" name="patient.id" component="select"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                valid={touched.patient && !errors.patient}
                                                                error={touched.patient && errors.patient}
                                                            >
                                                                <option value="">Choisir un Patient</option>
                                                                {patient.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>

                                                        </Label>

                                                    </Col>
                                                    <Col>
                                                        <Label htmlFor="medecin">
                                                            Medecin
                            <Input id="medecin.id" name="medecin.id" component="select"
                                                                valid={touched.medecin && !errors.medecin}
                                                                error={touched.medecin && errors.medecin}
                                                            >
                                                                <option value="">Choisir un Medecin</option>
                                                                {medecin.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>

                                                        </Label>


                                                        <Label htmlFor="diagnostic">
                                                            Diagnostic
                                <Input
                                                                type="text"
                                                                name="diagnostic"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Diagnostic"
                                                                valid={touched.diagnostic && !errors.diagnostic}
                                                                error={touched.diagnostic && errors.diagnostic}
                                                            />
                                                        </Label>



                                                        <Label htmlFor="prescribe">
                                                            Ordonnance
                                <Input id="prescribe.id" name="prescribe.id" component="select"
                                                                valid={touched.prescribe && !errors.prescribe}
                                                                error={touched.prescribe && errors.prescribe}
                                                            >
                                                                <option value="">Choisir une Ordonnance</option>
                                                                {prescribe.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>

                                                        </Label>


                                                        <Label htmlFor="diagnostics">
                                                            Diagnostic
                                <Input id="diagnostics.id" name="diagnostics.id" component="select"
                                                                valid={touched.diagnostics && !errors.diagnostics}
                                                                error={touched.diagnostics && errors.diagnostics}
                                                            >
                                                                <option value="">Choisir un Diagnostic</option>
                                                                {diagnostic.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>

                                                        </Label>
                                                        <Label htmlFor="conducted_physical_exam">
                                                            Resultat d'examen
                                <Input id="conducted_physical_exam.id" name="conducted_physical_exam.id" component="select"
                                                                valid={touched.conducted_physical_exam && !errors.conducted_physical_exam}
                                                                error={touched.conducted_physical_exam && errors.conducted_physical_exam}
                                                            >
                                                                <option value="">Choisir Resultat d'examen</option>
                                                                {result.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>

                                                        </Label>

                                                    </Col>


                                                </Row>

                                                <Label htmlFor="observation">
                                                    Observation
                            <Input
                                                        component="textarea"
                                                        name="observation"
                                                        autoCorrect="off"
                                                        autoComplete="name"
                                                        placeholder="Diagnostique"
                                                        valid={touched.observation && !errors.observation}
                                                        error={touched.observation && errors.observation}
                                                    />
                                                </Label>

                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={resetConsultation.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                            </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={ConsultationsListe.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Consultations
                            </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
