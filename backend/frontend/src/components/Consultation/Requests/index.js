import axios from "axios";

const url = axios.create({
    baseURL: "http://localhost:8080/"
});

export const getPatients = (body) => {
    let result = url
        .get("/patients")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getDrug = (body) => {
    let result = url
        .get("/drugs")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getMedecin = (body) => {
    let result = url
        .get("/medecins")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getPrescribe = (body) => {
    let result = url
        .get("/prescribes")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};
export const getDiagnostics = (body) => {
    let result = url
        .get("/diagnostics ")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};
export const getResult = (body) => {
    let result = url
        .get("/conducted_physical_exams")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};




