import React, {useCallback, useEffect, useRef, useState} from "react";
import axios from "axios";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
    faEdit,
    faPlusSquare,
    faDisease,
    faList,
    faStop,
    faHourglassStart,
    faUndo
} from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./style";


import http from "../../http-common";
import { getDiagnostics, getMedecin, getPatients, getPrescribe, getResult } from "./Requests";
import { confirmAlert } from "react-confirm-alert";
import organeService from "../../services/organeService";


const formSchema = Yup.object().shape({
    titre: Yup.string()
        .min(2, "C'est trop court")
        .max(8, "C'est trop long")
        .required("Veuillez saisir un titre "),
    observation: Yup.string()
        .min(2, "C'est trop court")
        .max(60, "C'est trop long")
        .required("Veuillez saisir une observation "),
    date: Yup.date().required("Veuillez saisir un date"),
    cost: Yup.number()
        .min(10, "C'est trop court")
        .max(200000, "C'est trop long")
        .required("Veuillez saisir un Coût"),
    diagnostic: Yup.string()
        .min(2, "C'est trop court")
        .max(30, "C'est trop long")
        .required("Veuillez saisir un diagnostique"),
    status: Yup.string()
        .required("Veuillez saisir un statut"),
    patient: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un patient')
    }),
    medecin: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un medecin')
    })
    ,
    prescribe: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir une ordonnance')
    })
    ,
    diagnostics: Yup.object().shape({
        id: Yup.string()
            .required('Veuillez saisir un diagnostique ')
    })
    ,
    conducted_physical_exam: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un resultat d'examen")
    })
});
export default function EditConsultation(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const [duration, setDuration] = useState(0);
    const [delay, setDelay] = useState(1000);
    const durationRef = useRef();

    const [errorRequest, setErrorRequest] = useState(false);
    const [patientList, setPatient] = useState([]);
    const [medecinList, setMedecin] = useState([]);
    const [prescribeList, setPrescribe] = useState([]);
    const [diagnosticList, setDiagnostic] = useState([]);
    const [resultList, setResult] = useState([]);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getPatients();
            response.errors ? setErrorRequest(true) : setPatient(response);
        }

        fetchInitialData();
    }, []);
    useEffect(() => {
        async function fetchInitialData() {
            const response = await getMedecin();
            response.errors ? setErrorRequest(true) : setMedecin(response);
        }

        fetchInitialData();
    }, []);
    useEffect(() => {
        async function fetchInitialData() {
            const response = await getPrescribe();
            response.errors ? setErrorRequest(true) : setPrescribe(response);
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getDiagnostics();
            response.errors ? setErrorRequest(true) : setDiagnostic(response);
        }

        fetchInitialData();
    }, []);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getResult();
            response.errors ? setErrorRequest(true) : setResult(response);
        }

        fetchInitialData();
    }, []);


    useInterval(() => setDuration((c) => c + 1), delay);

    useEffect(() => {
        if (delay == null) alert(`Duration ${duration}`);
    }, [duration, delay]);

    function useInterval(callback, delay) {
        const savedCallback = useRef();

        useEffect(() => {
            savedCallback.current = callback;
        });

        useEffect(() => {
            let id;
            if (delay) {
                id = setInterval(savedCallback.current, delay);
            }
            return () => clearInterval(id);
        }, [delay]);
    }


    const initialStates = {
        duration: "",
        patient: { id: "",nom: "" },
        medecin: { id: "",nom: "" },
        prescribe: { id: "",nom: "" },
        diagnostics: { id: "",nom: "" },
        conducted_physical_exam: { id: "",nom: "" },
        titre: "",
        observation: "",
        date: "",
        cost: "",
        diagnostic: "",
        status: ""
    }

    const [currentStates, setCurrentStates] = useState(initialStates);


    const getById = id => {
        const get = id => {
            return http.get(`/consultations/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };



    const handleChangePatient = event => {
        const patient = patientList.find(({ id }) => id === event.target.value);
        if(patient) setCurrentStates({ patient });
    };
    const handleChangeMedecin = event => {
        const medecin = medecinList.find(({ id }) => id === event.target.value);
        if(medecin) setCurrentStates({ medecin });
    };

    const handleChangeResult = event => {
        const result = resultList.find(({ id }) => id === event.target.value);
        if(result) setCurrentStates({ result });
    };

    const handleChangeDiagnostic = event => {
        const diagnostic = diagnosticList.find(({ id }) => id === event.target.value);
        if(diagnostic) setCurrentStates({ diagnostic });
    };

    const handleChangeOrdonnance = event => {
        const prescribe = prescribeList.find(({ id }) => id === event.target.value);
        if(prescribe) setCurrentStates({ prescribe });
    };






    const updateConsultation = () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.patch(`/consultations/${id}`, data);
                        };

                        update(currentStates.id, {
                            titre: currentStates.titre,
                            observation: currentStates.observation,
                            date: currentStates.date,
                            cost: currentStates.cost,
                            diagnostic: currentStates.diagnostic,
                            status: currentStates.status,
                            medecin: currentStates.medecin,
                            prescribe: currentStates.prescribe,
                            diagnostics: currentStates.diagnostics,
                            conducted_physical_exam: currentStates.conducted_physical_exam,
                            duration: duration



                        })
                            .then(response => {
                                props.history.push("/listcons");
                                alert("Consultation modifié  avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });
                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });


    };
    const Liste = () => {
        return props.history.push("/listcons");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Consultations</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Consultation
                                    </h3>
                                </div>

                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <div style={{ textAlign: 'center' }}>
                                                <h3>Duration</h3>
                                                <h2>{duration}</h2>
                                                <button className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }} onClick={() => setDuration(0)}>
                                                    <FontAwesomeIcon icon={faUndo} />  Reset
                                                </button>
                                                <button
                                                    className="btn btn-primary" style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={() => setDelay((delay) => (delay ? null : 1000))}
                                                >
                                                    <FontAwesomeIcon icon={delay ? faStop : faHourglassStart} /> {delay ? "Pause" : "Start"}
                                                </button>


                                            </div>
                                            <form>
                                                <Row>
                                                    <Col>

                                                        <Form.Group as={Col} controlId="formGridTitre">
                                                            <Form.Label>Titre</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="titre"
                                                                name="titre"
                                                                placeholder="titre"
                                                                value={currentStates.titre}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>


                                                        <Form.Group as={Col} controlId="formGridStatut">
                                                            <Form.Label>Statut</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="status"
                                                                name="status"
                                                                value={currentStates.status}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            >
                                                                <option value=" " >Choisir un Statut </option>
                                                                <option value="ST1">ST1</option>
                                                                <option value="ST2">ST2</option>
                                                            </Form.Control>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridDate">
                                                            <Form.Label>Date</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="date"
                                                                id="date"
                                                                name="date"
                                                                value={currentStates.date}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridCoût">
                                                            <Form.Label>Coût</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="number"
                                                                id="cost"
                                                                name="cost"
                                                                placeholder="Coût"
                                                                value={currentStates.cost}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridPatient">
                                                            <Form.Label>Patient</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="patient.id"
                                                                name="patient.id"
                                                                value={currentStates.patient?.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChangePatient}>

                                                                <option value="">Choisir un Patient</option>
                                                                {patientList.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>
                                                    </Col>
                                                    <Col>
                                                        <Form.Group as={Col} controlId="formGridMedecin">
                                                            <Form.Label>Medecin</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="medecin.id"
                                                                name="medecin.id"
                                                                value={currentStates.medecin?.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChangeMedecin}>

                                                                <option value="">Choisir un Medecin</option>
                                                                {medecinList.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridDiagnostic">
                                                            <Form.Label>Diagnostic</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="diagnostic"
                                                                name="diagnostic"
                                                                placeholder="Diagnostic"
                                                                value={currentStates.diagnostic}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridOrdonnance">
                                                            <Form.Label>Ordonnance</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="prescribe.id"
                                                                name="prescribe.id"
                                                                value={currentStates.prescribe?.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChangeOrdonnance}>

                                                                <option value="">Choisir une Ordonnance</option>
                                                                {prescribeList.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridDiagnostic">
                                                            <Form.Label>Diagnostics</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="diagnostics.id"
                                                                name="diagnostics.id"
                                                                value={currentStates.diagnostics?.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChangeDiagnostic}>

                                                                <option value="">Choisir un Diagnostic</option>
                                                                {diagnosticList.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridResultat">
                                                            <Form.Label>Resultat d'examen</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="conducted_physical_exam.id"
                                                                name="conducted_physical_exam.id"
                                                                value={currentStates.conducted_physical_exam?.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChangeResult}>

                                                                <option value="">Choisir un Resultat d'examen</option>
                                                                {resultList.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>

                                                    </Col>

                                                </Row>
                                                <Form.Group as={Col} controlId="formGridObservation">
                                                    <Form.Label>Observation</Form.Label>
                                                    <Form.Control required as="textarea"
                                                        type="text"
                                                        id="observation"
                                                        name="observation"
                                                        placeholder="observation"
                                                        value={currentStates.observation}
                                                        className={"bg-white text-dark"}
                                                        onChange={handleInputChange}
                                                    />
                                                </Form.Group>

                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateConsultation}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Consultations
                                                </Button>
                                            </Card.Footer>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};