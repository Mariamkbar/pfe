import React, { Component } from 'react';
import axios from "axios";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFilePdf, faFolderOpen, faHospitalUser, faList } from "@fortawesome/free-solid-svg-icons";
import { Button, Col, Row, Card } from "react-bootstrap";
import Printer, { print } from 'react-pdf-print';
import "./PdfCSS.css";
import ReactPaginate from "react-paginate";

const ids = ['5']







class ExamenPDF extends Component {
    constructor(props) {
        super(props);
        this.state = {
            patient: {},
            consultations: [],
            offset: 0,
            orgtableData: [],
            perPage: 1,
            currentPage: 0

        };
        this.handlePageClick = this.handlePageClick.bind(this);
    }
    componentDidMount() {
        this.getPatient()
        axios.get('/consultations/getConsultationByPatientId/' + this.props.match.params.id)
            .then(res => {
                this.setState({ consultations: res.data });
                console.log(this.state.consultations);

            });

        this.getData();

    }
    getPatient(){
        axios.get('/patients/' + this.props.match.params.id)
            .then(res => {
                this.setState({ patient: res.data });
                console.log(this.state.patient);
            });
    }

    handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * this.state.perPage;

        this.setState(
            {
                currentPage: selectedPage,
                offset: offset
            },
            () => {
                this.loadMoreData();
            }
        );
    };

    loadMoreData() {
        const data = this.state.orgtableData;

        const slice = data.slice(
            this.state.offset,
            this.state.offset + this.state.perPage
        );
        this.setState({
            pageCount: Math.ceil(data.length / this.state.perPage),
            consultations: slice
        });
    }
    getData() {
        axios.get('/consultations/getConsultationByPatientId/' + this.props.match.params.id).then((res) => {
            this.setState({ consultations: res.data });
            console.log(this.state.consultations);
            const data = res.data;

            const slice = data.slice(
                this.state.offset,
                this.state.offset + this.state.perPage
            );

            this.setState({
                pageCount: Math.ceil(data.length / this.state.perPage),
                orgtableData: res.data,
                consultations: slice
            });
        });


    }
    //Méthode bouton Dossier Médicale

    ShowPatient = () => {
        return this.props.history.push(`/show/${this.state.patient.id}`);
    };


    render() {
        return (

            <div className="container ">
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">

                            <div className="card bg-white">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Les resultats des examens
                                    </h3>
                                </div>


                                <div className="card-body bg-white">

                                    <div className='App'>
                                        <Button size="sm" variant="info" type="button" onClick={this.ShowPatient.bind()}>
                                            <FontAwesomeIcon icon={faFolderOpen} /> Dossier du patient
                                        </Button>{' '}
                                        <Button className="btn btn-sm btn-outline-primary" size="sm" variant="info" type="button" style={{ position: 'relative', float: 'right', "width": "120px", "margin": "1px", "padding": "2px" }}
                                                onClick={() => print(ids)}>
                                            <FontAwesomeIcon icon={faFilePdf} /> Imprimer Resultat
                                        </Button>
                                        <ReactPaginate
                                            previousLabel={"prev"}
                                            nextLabel={"next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={this.state.pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"}
                                        />
                                        <div className="table-responsive">


                                            <Printer>
                                                <div className="book">
                                                    <div className="page">
                                                        <div className="card border-info">


                                                            <Col style={{ "margin": "auto" }}>


                                                                <div id={ids[0]} style={{ width: '210mm', height: '297mm' }}>

                                                                    <Col style={{ "margin": "auto" }}>
                                                                        <div><p><strong>_____________________________________________________________________________________</strong></p></div>
                                                                        <img
                                                                            src={require('../Maladies/index.jpg')}
                                                                            alt="profile-img"
                                                                            width="50%" height="50%"
                                                                            className="profile-img-card"
                                                                        /><h1 style={{ "font-family": "cursive" }} >Le resulat de examen du patient </h1>
                                                                        <div><p ><strong>_____________________________________________________________________________________</strong></p></div>
                                                                        <div className="table-responsive">

                                                                            {this.state.consultations?.map(c =>
                                                                                <div>
                                                                                    <Row>
                                                                                        <Col>
                                                                                            <p style={{ "font-size": "20px","font-family": "cursive" }}><strong style={{ "color": "blue" }}>Nom du patient : </strong>{c.patient.nom}{' '} {c.patient.prenom} </p>
                                                                                            <p style={{ "font-size": "20px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}>Adresse du patient </strong></p>
                                                                                            <p style={{ "font-size": "20px" ,"font-family": "cursive"}}> <strong style={{ "color": "blue" }}>Téléphone :</strong>{c.patient.tel1}</p>
                                                                                            <p style={{ "font-size": "20px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Whatsapp :</strong> {c.patient.whatapp} </p>
                                                                                        </Col>
                                                                                        <Col>
                                                                                            <div >
                                                                                                <img class="rounded-circle" src={`data:image/png;base64,${c.patient.image.data}`} width="50%" height="50%" />

                                                                                            </div>
                                                                                        </Col>
                                                                                    </Row>
                                                                                    <div><p  ><strong>_____________________________________________________________________________________</strong></p></div>
                                                                                    <h1 style={{ "font-family": "cursive" }}>Examen info: </h1>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}> <strong style={{ "color": "blue" }}>Nom d'examen : </strong>{c.conducted_physical_exam?.physical_exam?.nom}</p>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}><strong style={{ "color": "blue" }}> Catagory d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.catagory}</p>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}><strong style={{ "color": "blue" }}> Sub Catagory d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.sub_catagory}</p>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Description d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.description}</p>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Unit d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.unit}</p>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}><strong style={{ "color": "blue" }}> Unit label d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.unit_label}</p>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Unit type d'examen :</strong>{c.conducted_physical_exam?.physical_exam?.unit_type}</p>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}><strong style={{ "color": "blue" }}> Nom d'organe :</strong>{c.conducted_physical_exam?.physical_exam?.organ.nom}</p>
                                                                                    <p style={{ "font-size": "17px","font-family": "cursive" }}><strong style={{ "color": "blue" }}> Result :</strong>{c.conducted_physical_exam?.result}</p>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Comment :</strong>{c.conducted_physical_exam?.comment}</p>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong style={{ "color": "blue" }}> Titre du Consultation :</strong>{c.titre}</p>
                                                                                    <div><p ><strong>_____________________________________________________________________________________</strong></p></div>
                                                                                    <p style={{ "font-size": "17px" ,"font-family": "cursive"}}><strong> Signature-------------------------------Date du Consultation :{c.date}</strong></p>



                                                                                </div>
                                                                            )}


                                                                        </div>
                                                                    </Col>
                                                                </div>
                                                            </Col>
                                                        </div>
                                                    </div >
                                                </div >
                                            </Printer>



                                        </div>

                                    </div>





                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
export default ExamenPDF;

