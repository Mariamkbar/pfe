import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faPlusSquare, faMicroscope, faSave, faUndo } from "@fortawesome/free-solid-svg-icons";
import { Button, Card, Col, Row } from "react-bootstrap";

import { getOrgan } from './Requests';

import {
    Label,
    Input,
    StyledInlineErrorMessage,
} from "./styles";
import { confirmAlert } from "react-confirm-alert";

const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un nom"),
    catagory: Yup.string()
        .required("Veuillez saisir une catagory"),
    sub_catagory: Yup.string()
        .required("Veuillez saisir un sub catagory"),
    description: Yup.string()
        .min(2, "C'est trop court")
        .max(500, "C'est trop long")
        .required("Veuillez saisir une description"),
    unit: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir une unit"),
    unit_label: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir une unit label"),
    unit_type: Yup.string()
        .required("Veuillez saisir une unit type"),
    organ: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un organ")
    })
});
export default function CreateExamen(props) {

    const [errorRequest, setErrorRequest] = useState(false);
    const [organ, setOrgan] = useState([]);
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };
    const handleOnSubmit = (values, actions) => {

        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/physical_exams", values
                        )
                            .then(response => {
                                props.history.push("/listexam");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                alert("Examen enregisté avec succés");
                                console.log(response.data);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getOrgan();
            response.errors ? setErrorRequest(true) : setOrgan(response);
        }

        fetchInitialData();
    }, []);

    const [initialValues, setInitialValues] = useState(
        {
            organ: { id: "" }, id: "", nom: "", catagory: "",
            sub_catagory: "", description: "", unit: "", unit_label: "", unit_type: ""
        }
    );
    const Liste = () => {
        return props.history.push("/listexam");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };


    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faMicroscope} /> Demande d'Examens</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Examen
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>
                                                        <Label htmlFor="organ">
                                                            Organ
                                            <Input id="organ.id" name="organ.id" component="select"
                                                                valid={touched.organ && !errors.organ}
                                                                error={touched.organ && errors.organ}
                                                            >
                                                                <option value="">Choisir un Organ</option>
                                                                {organ.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.organNom}
                                                                    </option>
                                                                ))}
                                                            </Input>
                                                        </Label>


                                                        <Label htmlFor="nom">
                                                            Nom
                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}


                                                        <Label htmlFor="catagory">
                                                            Sub Catagory
                                                            <Input id="sub_catagory" name="sub_catagory" component="select"
                                                                   valid={touched.sub_catagory && !errors.sub_catagory}
                                                                   error={touched.sub_catagory && errors.sub_catagory}>
                                                                <option value=" " >Choisir une Sub Catagory </option>
                                                                <option value="ECG">ECG</option>
                                                                <option value="Test PCR">Test PCR</option>
                                                                <option value="Tests hépatiques">Tests hépatiques</option>
                                                                <option value="Une image de sang qui révèle une anémie">Une image de sang qui révèle une anémie</option>
                                                                <option value="Tests de diabète">Tests de diabète</option>
                                                                <option value="Tests sanguins">Tests sanguins</option>
                                                                <option value="Rayons normaux (rayons X)">Rayons normaux (rayons X)</option>
                                                                <option value="Tomodensitométrie">Tomodensitométrie</option>
                                                                <option value="Ultrason">Ultrason</option>
                                                                <option value="résonance magnétique">résonance magnétique</option>
                                                            </Input>

                                                        </Label>

                                                        <Label htmlFor="catagory">
                                                            Catagory
                                                            <Input id="catagory" name="catagory" component="select"
                                                                   valid={touched.catagory && !errors.catagory}
                                                                   error={touched.catagory && errors.catagory}>
                                                                <option value=" " >Choisir une Catagory </option>
                                                                <option value="Analyses ">Analyses </option>
                                                                <option value="Radiographies">Radiographies</option>
                                                            </Input>

                                                        </Label>
                                                    </Col>
                                                    <Col>


                                                        <Label htmlFor="unit">
                                                            Unit
                                            <Input
                                                                type="text"
                                                                name="unit"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Unit"
                                                                valid={touched.unit && !errors.unit}
                                                                error={touched.unit && errors.unit}
                                                            />
                                                        </Label>
                                                        {errors.unit && touched.unit && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.unit}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="unit_label">
                                                            Unit Label
                                            <Input
                                                                type="text"
                                                                name="unit_label"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Unit Label"
                                                                valid={touched.unit_label && !errors.unit_label}
                                                                error={touched.unit_label && errors.unit_label}
                                                            />
                                                        </Label>
                                                        {errors.unit_label && touched.unit_label && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.unit_label}
                                                            </StyledInlineErrorMessage>
                                                        )}
                                                        <Label htmlFor="unit_type">
                                                            Unit Type
                                            <Input id="unit_type" name="unit_type" component="select"
                                                                valid={touched.unit_type && !errors.unit_type}
                                                                error={touched.unit_type && errors.unit_type}>
                                                                <option value=" " >Choisir un Unit Type </option>
                                                                <option value="N">N</option>
                                                                <option value="T">T</option>
                                                            </Input>

                                                        </Label>
                                                        {errors.unit_type && touched.unit_type && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.unit_type}
                                                            </StyledInlineErrorMessage>
                                                        )}
                                                        <Label htmlFor="description">
                                                            Description
                                                            <Input
                                                                component="textarea"
                                                                name="description"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Description"
                                                                valid={touched.description && !errors.description}
                                                                error={touched.description && errors.description}
                                                            />
                                                        </Label>
                                                        {errors.description && touched.description && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.description}
                                                            </StyledInlineErrorMessage>
                                                        )}
                                                    </Col>


                                                </Row>



                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Examens
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
