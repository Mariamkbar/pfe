import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faPlusSquare, faMicroscope, faSave, faUndo } from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from "react-bootstrap";

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";

import { getExamen } from './Requests';
import { confirmAlert } from "react-confirm-alert";

const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un nom"),
    result: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un examen result"),
    comment: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un comment"),
    physical_exam: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un examen")
    })
});
export default function CreateConducted_physical_exam(props) {

    const [errorRequest, setErrorRequest] = useState(false);
    const [exam, setExam] = useState([]);
    /* Server State Handling */

    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getExamen();
            response.errors ? setErrorRequest(true) : setExam(response);
        }

        fetchInitialData();
    }, []);

    const handleOnSubmit = (values, actions) => {
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/conducted_physical_exams", values
                        )
                            .then(response => {
                                props.history.push("/addres");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                alert("Resultat de L'Examen enregisté avec succés");
                                console.log(response.data);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });
                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const [initialValues, setInitialValues] = useState(
        {
            nom: "",result: "", comment: "", physical_exam: { id: "",nom: "" }
        }
    );
    const Liste = () => {
        return props.history.push("/addres");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faMicroscope} /> Gestion des Resultats d'Examens</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Resultat d'Examen
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>

                                                        <Label htmlFor="organ">
                                                            Examen
                                        <Input id="physical_exam.id" name="physical_exam.id" component="select"
                                                                valid={touched.physical_exam && !errors.physical_exam}
                                                                error={touched.physical_exam && errors.physical_exam}
                                                            >
                                                                <option value="">Choisir un Examen</option>
                                                                {exam.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>
                                                        </Label>




                                                        <Label htmlFor="result">
                                                            Resultat d'examen
                                        <Input
                                                                type="text"
                                                                name="result"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Resultat"
                                                                valid={touched.result && !errors.result}
                                                                error={touched.result && errors.result}
                                                            />
                                                        </Label>
                                                        {errors.result && touched.result && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.result}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                    </Col>
                                                    <Col>

                                                        <Label htmlFor="nom">
                                                            Nom
                                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}


                                                    <Label htmlFor="comment">
                                                        Comment
                                                        <Input
                                                            type="text"
                                                            name="comment"
                                                            autoCorrect="off"
                                                            autoComplete="name"
                                                            placeholder="Comment"
                                                            valid={touched.comment && !errors.comment}
                                                            error={touched.comment && errors.comment}
                                                        />
                                                    </Label>
                                                    {errors.comment && touched.result && (
                                                        <StyledInlineErrorMessage>
                                                            {errors.comment}
                                                        </StyledInlineErrorMessage>
                                                    )}
                                                    </Col>

                                                </Row>

                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Resultats
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
