import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlusSquare, faDisease, faList } from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';



import http from "../../http-common";
import { getExamen, getOrgan } from "./Requests";
import { Link } from "react-router-dom";
import { confirmAlert } from "react-confirm-alert";
import organeService from "../../services/organeService";


const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(20, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    result: Yup.string()
        .min(2, "C'est trop court")
        .max(10, "C'est trop long")
        .required("Veuillez saisir un examen result"),
    comment: Yup.string()
        .min(2, "C'est trop court")
        .max(10, "C'est trop long")
        .required("Veuillez saisir un comment"),
    physical_exam: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un examen")
    })
});
export default function EditExamResult(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const [errorRequest, setErrorRequest] = useState(false);
    const [examList, setExam] = useState([]);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getExamen();
            response.errors ? setErrorRequest(true) : setExam(response);
        }

        fetchInitialData();
    }, []);

    const initialStates = {
        nom: "", result: "", comment: "", physical_exam: { id: "",nom: "" }
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        const get = id => {
            return http.get(`/conducted_physical_exams/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };
    const handleChange = event => {
        const exam = examList.find(({ id }) => id === event.target.value);
        if(exam) setCurrentStates({ exam });
    };





    const updateExamen = () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.put(`/conducted_physical_exams/${id}`, data);
                        };
                        update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/addres");
                                alert("Resultat d'Examen modifié avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };

    const Liste = () => {
        return props.history.push("/addres");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Examens</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Résultat d'Examen
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Row>
                                                    <Col>

                                                    <Form.Group as={Col} controlId="formGridIo">
                                                        <Form.Label>Examen</Form.Label>
                                                        <Form.Control required as="select"
                                                            type="text"
                                                            id="physical_exam.id"
                                                            name="physical_exam.id"
                                                            value={currentStates.physical_exam?.id}
                                                            className={"bg-white text-dark"}
                                                            onChange={handleChange}>

                                                            <option value="">Choisir un Examen</option>
                                                            {examList.map((value) => (
                                                                <option value={value.id} key={value.id}>
                                                                    {value.nom}
                                                                </option>
                                                            ))}

                                                        </Form.Control>

                                                    </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridNom">
                                                            <Form.Label>Nom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                          type="text"
                                                                          id="nom"
                                                                          name="nom"
                                                                          placeholder="Nom"
                                                                          value={currentStates.nom}
                                                                          className={"bg-white text-dark"}
                                                                          onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        </Col>
                                                    <Col>


                                                    <Form.Group as={Col} controlId="formGridResult">
                                                        <Form.Label>Result</Form.Label>
                                                        <Form.Control required autoComplete="off"
                                                            type="text"
                                                            id="result"
                                                            name="result"
                                                            placeholder="Result"
                                                            value={currentStates.result}
                                                            className={"bg-white text-dark"}
                                                            onChange={handleInputChange}
                                                        />
                                                    </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridComment">
                                                            <Form.Label>Comment</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                          type="text"
                                                                          id="comment"
                                                                          name="comment"
                                                                          placeholder="Comment"
                                                                          value={currentStates.comment}
                                                                          className={"bg-white text-dark"}
                                                                          onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        </Col>

                                                </Row>


                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateExamen}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>{' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Resultats
                                                </Button>
                                            </Card.Footer>
                                            <p>{message}</p>
                                        </div>

                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
