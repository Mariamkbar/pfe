import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faList, faTrash, faMicroscope, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { Button, ButtonGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function ExamensList(props) {

    const url = '/physical_exams'
    const [data, setData] = useState([])
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(3);
    const [pageCount, setPageCount] = useState(0)
    const [orgtableData, setOrgtableData] = useState([]);

    const loadMoreData = () => {
        const data = orgtableData;

        const slice = data.slice(
            offset,
            offset + perPage
        );

        setPageCount(Math.ceil(data.length / perPage))
        setData(slice)
    }

    const getData = async () => {
        axios.get(`/physical_exams`).then((res) => {
            setData(res.data);
            console.log(data);
            const data = res.data;

            const slice = data.slice(offset, offset + perPage)

            setPageCount(Math.ceil(data.length / perPage))
            setOrgtableData(res.data)
            setData(slice)
        });

    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setPageCount(selectedPage);
        setOffset(offset, () => {
            loadMoreData();
        });



    };

    const CreateExamen = () => {
        return props.history.push("/addexamans");
    };


    useEffect(() => {
        retrievePhysical_exams();
        getData()

    }, [offset]

    )



    const retrievePhysical_exams = () => {
        axios.get(url).then(json => setData(json.data))

    };

    const refreshList = () => {
        retrievePhysical_exams();

    };

    const deletePhysical_exams = (id) => {
        console.log(id);
        axios.delete(`/physical_exams/${id}`)
            .then((result) => {
                if (result.data != null) {
                    alert('Examen été suprimer!!');
                }
                refreshList();
            });

    };

    const renderTable = () => {
        return data?.map(c => {
            return (
                <tr>
                    <td>{c.nom}</td>
                    <td>{c.catagory}</td>
                    <td>{c.sub_catagory}</td>
                    <td>{c.description}</td>
                    <td>{c.unit}</td>
                    <td>{c.unit_label}</td>
                    <td>{c.unit_type}</td>
                    <td>{c.organ?.organNom}</td>
                    <td>
                        <ButtonGroup>
                            <Link to={`/editexamans/${c.id}`} className="btn btn-sm btn-outline-primary">
                                <FontAwesomeIcon icon={faEdit} /></Link>&nbsp;

                            {/*   <Button size="sm" variant="outline-danger"
                                onClick={() => {
                                    if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?'))
                                        deletePhysical_exams(c.id)
                                }}><FontAwesomeIcon icon={faTrash} />
                            </Button>*/}
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }
    return (
        <div>
            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faMicroscope} /> Demande d'Examens</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Exemens
                                            <Button size="sm" variant="info" type="button" style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }}
                                                onClick={CreateExamen.bind()}>
                                                <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Exemen
                                            </Button>
                                        </h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        <table class="table table-stripe"  >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort">Nom</th>
                                                    <th scope="col" className="sort">Catagory</th>
                                                    <th scope="col" className="sort">Sub Catagory</th>
                                                    <th scope="col" className="sort">Description</th>
                                                    <th scope="col" className="sort">Unit</th>
                                                    <th scope="col" className="sort">Unit Label</th>
                                                    <th scope="col" className="sort">Unit Type</th>
                                                    <th scope="col" className="sort">Organ</th>
                                                    <th scope="col" className="sort">Action</th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">{renderTable()}</tbody>
                                        </table>
                                        <ReactPaginate
                                            previousLabel={"prev"}
                                            nextLabel={"next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"} />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
