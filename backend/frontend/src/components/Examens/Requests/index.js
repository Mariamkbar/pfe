import axios from "axios";

const url = axios.create({
    baseURL: "http://localhost:8080/"
});
export const getExamen = (body) => {
    let result = url
        .get("/physical_exams")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

export const getOrgan = (body) => {
    let result = url
        .get("/organs")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};

