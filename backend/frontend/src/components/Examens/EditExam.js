import React, { useEffect, useState } from "react";

import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlusSquare, faDisease, faList } from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';



import http from "../../http-common";
import { getOrgan } from "./Requests";
import { confirmAlert } from "react-confirm-alert";


const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(20, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(20, "C'est trop long")
        .required("Veuillez saisir un nom")
});
export default function EditExam(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const [errorRequest, setErrorRequest] = useState(false);
    const [organlist, setOrgan] = useState([]);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getOrgan();
            response.errors ? setErrorRequest(true) : setOrgan(response);
        }

        fetchInitialData();
    }, []);

    const initialStates = {
        id: "", nom: "", organ: { id: "",organNom: "" }, catagory: "",
        sub_catagory: "", description: "", unit: "",
        unit_label: "", unit_type: ""
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        const get = id => {
            return http.get(`/physical_exams/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };
    const handleChange = event => {
        const organ = organlist.find(({ id }) => id === event.target.value);
        if(organ) setCurrentStates({ organ });
    };





    const updateExamen = () => {
        console.log('Form Data',currentStates);
        alert(JSON.stringify(currentStates, null, 5))
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.patch(`/physical_exams/${id}`, data);
                        };
                        update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/listexam");
                                alert("Examen modifier avec succer");
                                console.log(response.data);
                                setMessage("l'examen modifié avec succés!");
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };

    const Liste = () => {
        return props.history.push("/listexam");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Examens</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Examen
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Row>
                                                    <Col>
                                                        <Form.Group as={Col} controlId="formGridIo">
                                                            <Form.Label>Organ</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="organ.id"
                                                                name="organ.id"
                                                                value={currentStates.organ.id}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleChange}>

                                                                <option value="">Choisir un Organ</option>
                                                                {organlist.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.organNom}
                                                                    </option>
                                                                ))}

                                                            </Form.Control>

                                                        </Form.Group>


                                                        <Form.Group as={Col} controlId="formGridNom">
                                                            <Form.Label>Nom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="nom"
                                                                name="nom"
                                                                placeholder="Nom"
                                                                value={currentStates.nom}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>
                                                        <Form.Group as={Col} controlId="formGridCatagory">
                                                            <Form.Label>Catagory</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="catagory"
                                                                name="catagory"
                                                                value={currentStates.catagory}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            >
                                                                <option value=" " >Choisir une Catagory </option>
                                                                <option value="Analyses ">Analyses </option>
                                                                <option value="Radiographies">Radiographies</option>
                                                            </Form.Control>
                                                        </Form.Group>


                                                        <Form.Group as={Col} controlId="formGridSubCatagory">
                                                            <Form.Label>Sub Catagory</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="sub_catagory"
                                                                name="sub_catagory"
                                                                value={currentStates.sub_catagory}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            >
                                                                <option value=" " >Choisir une Sub Catagory </option>
                                                                <option value="ECG">ECG</option>
                                                                <option value="Test PCR">Test PCR</option>
                                                                <option value="Tests hépatiques">Tests hépatiques</option>
                                                                <option value="Une image de sang qui révèle une anémie">Une image de sang qui révèle une anémie</option>
                                                                <option value="Tests de diabète">Tests de diabète</option>
                                                                <option value="Tests sanguins">Tests sanguins</option>
                                                                <option value="Rayons normaux (rayons X)">Rayons normaux (rayons X)</option>
                                                                <option value="Tomodensitométrie">Tomodensitométrie</option>
                                                                <option value="Ultrason">Ultrason</option>
                                                                <option value="résonance magnétique">résonance magnétique</option>
                                                            </Form.Control>
                                                        </Form.Group>
                                                    </Col>
                                                    <Col>


                                                        <Form.Group as={Col} controlId="formGridUnit">
                                                            <Form.Label>Unit</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="unit"
                                                                name="unit"
                                                                placeholder="Unit"
                                                                value={currentStates.unit}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridUnitLabel">
                                                            <Form.Label>Unit Label</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="unit_label"
                                                                name="unit_label"
                                                                placeholder="Unit Label"
                                                                value={currentStates.unit_label}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridUnitType">
                                                            <Form.Label>Unit Type</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="unit_type"
                                                                name="unit_type"
                                                                value={currentStates.unit_type}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            >
                                                                <option value=" " >Choisir un Unit Type </option>
                                                                <option value="N">N</option>
                                                                <option value="T">T</option>
                                                            </Form.Control>
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridDescription">
                                                            <Form.Label>Description</Form.Label>
                                                            <Form.Control required as="textarea"
                                                                type="text"
                                                                id="description"
                                                                name="description"
                                                                placeholder="Description"
                                                                value={currentStates.description}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                    </Col>
                                                </Row>

                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateExamen}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                            </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Examens
                                                </Button>
                                            </Card.Footer>
                                            <p>{message}</p>
                                        </div>

                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
