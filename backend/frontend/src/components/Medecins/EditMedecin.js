import React, { useEffect, useState } from "react";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faPlusSquare, faDisease, faList } from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';


import http from "../../http-common";
import { confirmAlert } from "react-confirm-alert";
import organeService from "../../services/organeService";



const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(15, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(15, "C'est trop long")
        .required("Veuillez saisir un nom"),
    specialite: Yup.string()
        .required("Veuillez saisir une spécialite"),
    address: Yup.string()
        .email("Invalid email")
        .min(6, "C'est trop court")
        .max(15, "C'est trop long")
        .required("Veuillez saisir un address"),
    tel: Yup.number()
        .min(10000000, "C'est trop court")
        .max(200000000000000, "C'est trop long")
        .required("Veuillez saisir un Telephone"),
});
export default function EditMedecin(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };


    const initialStates = {
        id: "", nom: "", specialite: "", address: "", tel: ""
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        const get = id => {
            return http.get(`/medecins/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };





    const updateExamen = () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.put(`/medecins/${id}`, data);
                        };
                        update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/listmedecins");
                                alert("Medecin modifié avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const Liste = () => {
        return props.history.push("/listmedecins");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Medecins</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Medecin
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Row>
                                                    <Col>

                                                        <Form.Group as={Col} controlId="formGridNom">
                                                            <Form.Label>Nom</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="text"
                                                                id="nom"
                                                                name="nom"
                                                                placeholder="Nom"
                                                                value={currentStates.nom}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridSp">
                                                            <Form.Label>Specialite</Form.Label>
                                                            <Form.Control required as="select"
                                                                type="text"
                                                                id="specialite"
                                                                name="specialite"
                                                                value={currentStates.specialite}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            >
                                                                <option value=" " >Choisir une spécialite  </option>
                                                                <option value="Généraliste">Généraliste</option>
                                                                <option value="Allergologie">Allergologie</option>
                                                                <option value="Andrologie">Andrologie</option>
                                                                <option value="Anesthésiologie">Anesthésiologie</option>
                                                                <option value="Cardiologie">Cardiologie</option>
                                                                <option value="Chirurgie">Chirurgie</option>
                                                                <option value="Neurochirurgie">Neurochirurgie</option>
                                                                <option value="Dermatologie">Dermatologie</option>
                                                                <option value="Gynécologie">Gynécologie</option>
                                                                <option value="Infectiologie">Infectiologie</option>
                                                            </Form.Control>
                                                        </Form.Group>


                                                    </Col>
                                                    <Col>
                                                        <Form.Group as={Col} controlId="formGridAddress">
                                                            <Form.Label>Address</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="email"
                                                                id="address"
                                                                name="address"
                                                                placeholder="Address"
                                                                value={currentStates.address}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>

                                                        <Form.Group as={Col} controlId="formGridTel">
                                                            <Form.Label>Téléphoune</Form.Label>
                                                            <Form.Control required autoComplete="off"
                                                                type="number"
                                                                id="tel"
                                                                name="tel"
                                                                placeholder="Téléphoune"
                                                                value={currentStates.tel}
                                                                className={"bg-white text-dark"}
                                                                onChange={handleInputChange}
                                                            />
                                                        </Form.Group>


                                                    </Col>


                                                </Row>

                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateExamen}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Medecins
                                                </Button>
                                            </Card.Footer>
                                            <p>{message}</p>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
