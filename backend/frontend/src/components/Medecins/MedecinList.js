import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faList, faPlusSquare, faTrash, faUserMd } from "@fortawesome/free-solid-svg-icons";
import { Button, ButtonGroup } from "react-bootstrap";
import { Link } from "react-router-dom";
import ReactPaginate from "react-paginate";

export default function MedecinList(props) {

    const url = '/medecins'
    const [data, setData] = useState([])
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(3);
    const [pageCount, setPageCount] = useState(0)
    const [orgtableData, setOrgtableData] = useState([]);
    const loadMoreData = () => {
        const data = orgtableData;

        const slice = data.slice(
            offset,
            offset + perPage
        );

        setPageCount(Math.ceil(data.length / perPage))
        setData(slice)
    }

    const getData = async () => {
        axios.get(`/medecins`).then((res) => {
            setData(res.data);
            console.log(data);
            const data = res.data;

            const slice = data.slice(offset, offset + perPage)

            setPageCount(Math.ceil(data.length / perPage))
            setOrgtableData(res.data)
            setData(slice)
        });

    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setPageCount(selectedPage);
        setOffset(offset, () => {
            loadMoreData();
        });



    };

    const CreateMedecin = () => {
        return props.history.push("/addmedecins");
    };

    useEffect(() => {
        retrieveMedecin();
            getData();
    }, [offset]

    )



    const retrieveMedecin = () => {
        axios.get(url).then(json => setData(json.data))

    };

    const refreshList = () => {
        retrieveMedecin();

    };

    const deleteMedecin = (id) => {
        console.log(id);
        axios.delete(`/medecins/${id}`)
            .then((result) => {
                if (result.data != null) {
                    alert('Medecin  été suprimer!!');
                }
                refreshList();
            });

    };

    const renderTable = () => {
        return data.map(con => {
            return (
                <tr>
                    <td>{con.nom}</td>
                    <td>{con.specialite}</td>
                    <td>{con.address}</td>
                    <td>{con.tel}</td>
                    <td>
                        <ButtonGroup>
                            <Link to={`/editmedecin/${con.id}`} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>&nbsp;
                            {/* <Button size="sm" variant="outline-danger" onClick={() => { if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?')) deleteMedecin(con.id) }}><FontAwesomeIcon icon={faTrash} /></Button>*/}
                        </ButtonGroup>
                    </td>
                </tr>
            )
        })
    }
    return (
        <div>
            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faUserMd} /> Gestion des Medecins</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Medecins
                                            <Button size="sm" variant="info" type="button" style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }}
                                                onClick={CreateMedecin.bind()}>
                                                <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Medecin
                                            </Button>
                                        </h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        <table class="table table-stripe"  >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort">Nom</th>
                                                    <th scope="col" className="sort">Specialite </th>
                                                    <th scope="col" className="sort">Address </th>
                                                    <th scope="col" className="sort">Telephon </th>
                                                    <th scope="col" className="sort">Action </th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">{renderTable()}</tbody>
                                        </table>
                                        <ReactPaginate
                                            previousLabel={"prev"}
                                            nextLabel={"next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"} />



                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
