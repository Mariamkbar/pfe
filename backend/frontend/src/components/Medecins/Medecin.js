import React, { useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faList, faPlusSquare, faSave, faUndo, faUserMd } from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';
import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";
import { confirmAlert } from "react-confirm-alert";

const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un nom"),
    specialite: Yup.string()
        .required("Veuillez saisir une spécialite"),
    address: Yup.string()
        .email("Invalid email")
        .min(6, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un address"),
    tel: Yup.number()
        .min(10000000, "C'est trop court")
        .max(200000000000000, "C'est trop long")
        .required("Veuillez saisir un Telephone"),
});
export default function Medecin(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };
    const handleOnSubmit = (values, actions) => {
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/medecins", values
                        )
                            .then(response => {
                                props.history.push("/listmedecins");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                alert("Medecin enregisté avec succés");
                                console.log(response.data);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });
                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });
    };
    const [initialValues, setInitialValues] = useState(
        {
             nom: "", specialite: "", address: "", tel: ""
        }
    );
    const Liste = () => {
        return props.history.push("/listmedecins");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faUserMd} /> Gestion des Medecins</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Medecin
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>
                                                        <Label htmlFor="code">
                                                            Nom
                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="tel">
                                                            Téléphone
                                                            <Input
                                                                type="number"
                                                                name="tel"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Téléphone"
                                                                valid={touched.tel && !errors.tel}
                                                                error={touched.tel && errors.tel}
                                                            />
                                                        </Label>
                                                        {errors.tel && touched.tel && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.tel}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                    </Col>
                                                    <Col>

                                                        <Label htmlFor="address">
                                                            Address
                                            <Input
                                                                type="email"
                                                                name="address"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Address"
                                                                valid={touched.address && !errors.address}
                                                                error={touched.address && errors.address}
                                                            />
                                                        </Label>
                                                        {errors.address && touched.address && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.address}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="specialite">
                                                            Spécialite
                                                            <Input id="specialite" name="specialite" component="select"
                                                                   valid={touched.specialite && !errors.specialite}
                                                                   error={touched.specialite && errors.specialite}>
                                                                <option value=" " >Choisir une spécialite  </option>
                                                                <option value="Généraliste">Généraliste</option>
                                                                <option value="Allergologie">Allergologie</option>
                                                                <option value="Andrologie">Andrologie</option>
                                                                <option value="Anesthésiologie">Anesthésiologie</option>
                                                                <option value="Cardiologie">Cardiologie</option>
                                                                <option value="Chirurgie">Chirurgie</option>
                                                                <option value="Neurochirurgie">Neurochirurgie</option>
                                                                <option value="Dermatologie">Dermatologie</option>
                                                                <option value="Gynécologie">Gynécologie</option>
                                                                <option value="Infectiologie">Infectiologie</option>
                                                            </Input>

                                                        </Label>
                                                    </Col>

                                                </Row>



                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Medecins
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
