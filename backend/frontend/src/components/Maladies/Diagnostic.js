import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare, faDisease, faSave, faUndo, faList } from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';

import { getMaladie } from './Requests';

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";
import { confirmAlert } from "react-confirm-alert";


const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(300, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    comment: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un comment"),
    malady: Yup.object().shape({
        id: Yup.string().required('Veuillez choisir une maladie')
    })
});
export default function CreateDiagnostic(props) {

    const [errorRequest, setErrorRequest] = useState(false);
    const [maladie, setMaladie] = useState([]);
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getMaladie();
            response.errors ? setErrorRequest(true) : setMaladie(response);
        }

        fetchInitialData();
    }, []);

    const handleOnSubmit = (values, actions) => {
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/diagnostics", values
                        )
                            .then(response => {
                                props.history.push("/listDiag");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                alert("Diagnostic enregisté avec succés");
                                console.log(response.data);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const [initialValues, setInitialValues] = useState(
        {
            nom: "", comment: "", malady: { id: "" }
        }
    );
    const Liste = () => {
        return props.history.push("/listDiag");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Faire des Diagnostics</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Diagnostic
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>

                                                        <Label htmlFor="malady">
                                                             Maladie
                                                            <Input id="malady.id" name="malady.id" component="select"
                                                                valid={touched.malady && !errors.malady}
                                                                error={touched.malady && errors.malady}
                                                            >
                                                                <option value="">Choisir une Maladie</option>
                                                                {maladie.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.nom}
                                                                    </option>
                                                                ))}
                                                            </Input>
                                                            <ErrorMessage name="malady.id" />
                                                        </Label>

                                                    </Col>
                                                    <Col>

                                                        <Label htmlFor="nom">
                                                            Nom
                                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}
                                                    </Col>



                                                </Row>

                                                <Label htmlFor="comment">
                                                    Comment
                                                    <Input
                                                        component="textarea"
                                                        name="comment"
                                                        autoCorrect="off"
                                                        autoComplete="name"
                                                        placeholder="Comment"
                                                        valid={touched.comment && !errors.comment}
                                                        error={touched.comment && errors.comment}
                                                    />

                                                    {errors.comment && touched.comment && (
                                                        <StyledInlineErrorMessage>
                                                            {errors.comment}
                                                        </StyledInlineErrorMessage>
                                                    )}
                                                </Label>
                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                                    </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                                    </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Diagnostiques
                                                    </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};