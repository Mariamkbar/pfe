import React, {useEffect, useState} from "react";
import axios from "axios";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faEdit, faPlusSquare, faDisease, faList} from "@fortawesome/free-solid-svg-icons";
import {Form, Card, Row, Col, Button} from 'react-bootstrap';

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";


import http from "../../http-common";
import {confirmAlert} from "react-confirm-alert";
import organeService from "../../services/organeService";
import {getOrgan} from "../Examens/Requests";



const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(8,"C'est trop long")
        .required("Veuillez saisir un identifiant"),
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(8,"C'est trop long")
        .required("Veuillez saisir un nom"),
    code: Yup.string()
        .min(2, "C'est trop court")
        .max(8,"C'est trop long")
        .required("Veuillez saisir un code"),
    catagory: Yup.string()
        .required("Veuillez saisir une catagory"),
    description: Yup.string()
        .min(2, "C'est trop court")
        .max(200,"C'est trop long")
        .required("Veuillez saisir une description"),
    sub_catagory: Yup.string()
        .required("Veuillez saisir une sub catagory")
});
export default function MaladyEdit(props)  {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ok, msg});
    };

    const [errorRequest, setErrorRequest] = useState(false);
    const [organList, setOrgan] = useState([]);

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getOrgan();
            response.errors ? setErrorRequest(true) : setOrgan(response);
        }

        fetchInitialData();
    }, []);



    const initialStates={
        id: "", nom: "", code: "", catagory: "", description: "", sub_catagory: "",organ: {organNom: "",id: "" }
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        const get = id => {
            return http.get(`/maladies/${id}`);
        };
        get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };
    const handleChange = event => {
        const organ = organList.find(({ id }) => id === event.target.value);
        if(organ) setCurrentStates({ organ });
    };





    const updateMalady= () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        const update = (id, data) => {
                            return http.put(`/maladies/${id}`, data);
                        };
                        update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/listmaladies");
                                alert("Maladie modifié avec succés");
                                console.log(response.data);
                            })
                            .catch(e => {
                                console.log(e);
                            });
                        alert('Click Oui')}
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };
    const Liste = () => {
        return props.history.push("/listmaladies");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Maladies</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Maladie
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Row>
                                                <Col>
                                                    <Form.Group as={Col} controlId="formGridIo">
                                                        <Form.Label>Code Organ</Form.Label>
                                                        <Form.Control required as="select"
                                                                      type="text"
                                                                      id="organ.id"
                                                                      name="organ.id"
                                                                      value={currentStates.organ?.id}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleChange}>

                                                            <option value="">Choisir un Organ</option>
                                                            {organList.map((value) => (
                                                                <option value={value.id} key={value.id}>
                                                                    {value.organNom}
                                                                </option>
                                                            ))}

                                                        </Form.Control>

                                                    </Form.Group>


                                                    <Form.Group as={Col} controlId="formGridNom">
                                                        <Form.Label>Nom</Form.Label>
                                                        <Form.Control required autoComplete="off"
                                                                      type="text"
                                                                      id="nom"
                                                                      name="nom"
                                                                      placeholder="Nom"
                                                                      value={currentStates.nom}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        />
                                                    </Form.Group>
                                                    <Form.Group as={Col} controlId="formGridCode">
                                                        <Form.Label>Code</Form.Label>
                                                        <Form.Control required autoComplete="off"
                                                                      type="number"
                                                                      id="code"
                                                                      name="code"
                                                                      placeholder="Code"
                                                                      value={currentStates.code}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        />
                                                    </Form.Group>
                                                </Col>
                                                <Col>
                                                    <Form.Group as={Col} controlId="formGridCatagory">
                                                        <Form.Label>Catagory</Form.Label>
                                                        <Form.Control required as="select"
                                                                      type="text"
                                                                      id="catagory"
                                                                      name="catagory"
                                                                      value={currentStates.catagory}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        >
                                                            <option value=" " >Choisir une Catagory </option>
                                                            <option value="Maladies de l'oreille, du nez et de la gorge">Maladies de l'oreille, du nez et de la gorge</option>
                                                            <option value="Coeur et système circulatoire">Coeur et système circulatoire</option>
                                                            <option value="Maladies infectieuses">Maladies infectieuses</option>
                                                            <option value="les maladies du sang">les maladies du sang</option>
                                                            <option value="Diabète">Diabète</option>
                                                            <option value="Maladies de la poitrine">Maladies de la poitrine</option>
                                                            <option value="Maladie du foie">Maladie du foie</option>
                                                        </Form.Control>
                                                    </Form.Group>

                                                    <Form.Group as={Col} controlId="formGridSubCatagory">
                                                        <Form.Label>Sub Catagory</Form.Label>
                                                        <Form.Control required as="select"
                                                                      type="text"
                                                                      id="sub_catagory"
                                                                      name="sub_catagory"
                                                                      value={currentStates.sub_catagory}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        >
                                                            <option value=" " >Choisir une Sub Catagory </option>
                                                            <option value="L'inflammation de l'oreille moyenne">L'inflammation de l'oreille moyenne</option>
                                                            <option value="Les maladies cardiaques">Les maladies cardiaques</option>
                                                            <option value="Covid 19">Covid 19</option>
                                                            <option value="VIH">VIH</option>
                                                            <option value="Paludisme">Paludisme</option>
                                                            <option value="L'anémie falciforme">L'anémie falciforme</option>
                                                            <option value="Injection Victoza">Injection Victoza</option>
                                                            <option value="Diabète de type 1">Diabète de type 1</option>
                                                            <option value="Diabète de type 2">Diabète de type 2</option>
                                                            <option value="Asthme">Asthme</option>
                                                            <option value="Maladie du foie">Maladie du foie B</option>
                                                            <option value="Maladie du foie">Maladie du foie C</option>
                                                        </Form.Control>
                                                    </Form.Group>

                                                    <Form.Group as={Col} controlId="formGridDescription">
                                                        <Form.Label>Description</Form.Label>
                                                        <Form.Control required as="textarea"
                                                                      type="text"
                                                                      id="description"
                                                                      name="description"
                                                                      placeholder="Description"
                                                                      value={currentStates.description}
                                                                      className={"bg-white text-dark"}
                                                                      onChange={handleInputChange}
                                                        />
                                                    </Form.Group>
                                                </Col>


                                                </Row>


                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateMalady}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                                </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des maladies
                                                </Button>
                                            </Card.Footer>
                                            <p>{message}</p>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
