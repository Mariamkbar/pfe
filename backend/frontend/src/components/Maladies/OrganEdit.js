import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare, faDisease, faEdit, faList } from "@fortawesome/free-solid-svg-icons";
import { Form, Card, Row, Col, Button } from 'react-bootstrap';

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";


import organeService from "../../services/organeService";
import { confirmAlert } from "react-confirm-alert";


const formSchema = Yup.object().shape({
    id: Yup.string()
        .min(2, "C'est trop court")
        .max(20, "C'est trop long")
        .required("Veuillez saisir un identifiant"),
    organNom: Yup.string()
        .min(2, "C'est trop court")
        .max(20, "C'est trop long")
        .required("Veuillez saisir un nom")
});
export default function OrganEdit(props) {
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };

    const initialStates = {
        id: "", organNom: ""
    }

    const [currentStates, setCurrentStates] = useState(initialStates);
    const [message, setMessage] = useState("");


    const getById = id => {
        organeService.get(id)
            .then(response => {
                setCurrentStates(response.data);
                console.log(response.data);
            })
            .catch(e => {
                console.log(e);
            });
    };

    useEffect(() => {
        getById(props.match.params.id);
    }, [props.match.params.id]);

    const handleInputChange = event => {
        const { name, value } = event.target;
        setCurrentStates({ ...currentStates, [name]: value });
    };





    const updateOrgane = () => {
        confirmAlert({

            title: 'Confirmer pour modifier',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        organeService.update(currentStates.id, currentStates)
                            .then(response => {
                                props.history.push("/listorgan");
                                alert("Organ modifier avec succer");
                                console.log(response.data);
                                setMessage("l'ogane modifié avec succés!");
                            })
                            .catch(e => {
                                console.log(e);
                            });

                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });


    };
    const Liste = () => {
        return props.history.push("/listorgan");
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Maladies</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Modifier Organ
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    {currentStates && (
                                        <div className="edit-form">
                                            <form>
                                                <Form.Group as={Col} controlId="formGridNom">
                                                    <Form.Label>Nom</Form.Label>
                                                    <Form.Control required autoComplete="off"
                                                        type="text"
                                                        id="organNom"
                                                        name="organNom"
                                                        value={currentStates.organNom}
                                                        className={"bg-white text-dark"}
                                                        onChange={handleInputChange}
                                                    />
                                                </Form.Group>



                                            </form>
                                            <Card.Footer style={{ "textAlign": "right" }}>
                                                <button
                                                    type="submit" class="btn btn-success"
                                                    style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                    onClick={updateOrgane}
                                                >
                                                    <FontAwesomeIcon icon={faEdit} /> Modifier
                                            </button>
                                                {' '}
                                                <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                    <FontAwesomeIcon icon={faList} /> liste des Organes
                                                </Button>
                                            </Card.Footer>
                                        </div>
                                    )}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
