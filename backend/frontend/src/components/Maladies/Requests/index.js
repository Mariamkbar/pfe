import axios from "axios";

const url = axios.create({
    baseURL: "http://localhost:8080/"
});
export const getMaladie = (body) => {
    let result = url
        .get("/maladies")
        .then((response) => {
            return response.data;
        })
        .catch((error) => {
            return error;
        });

    return result;
};


