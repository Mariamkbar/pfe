import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEdit, faList, faTrash, faDisease, faPen, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { Button, ButtonGroup, Card } from "react-bootstrap";
import { Link } from "react-router-dom";

import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css

import ReactPaginate from 'react-paginate';
import './App.css'

export default function OrganList(props) {

    const url = '/organs'
    const [data, setData] = useState([])
    const [offset, setOffset] = useState(0);
    const [perPage] = useState(2);
    const [pageCount, setPageCount] = useState(0)
    const [orgtableData, setOrgtableData] = useState([])

    useEffect(() => {
        retrieveOrgan();
        getData();
    }, [offset])

    useEffect(() => {
        getData();
    }, [offset])


    const getData = () => {
        axios.get(`/organs`).then((res) => {
            const data = res.data;
            console.log(data);
            const slice = data.slice(offset, offset + perPage)

            setPageCount(Math.ceil(data.length / perPage))
            setOrgtableData(res.data);
            setData(slice);
        });
    }
    const handlePageClick = (e) => {
        const selectedPage = e.selected;
        const offset = selectedPage * perPage;
        setPageCount(selectedPage);
        setOffset(offset, () => {
            loadMoreData();
        });



    };
    const loadMoreData = () => {
        const data = orgtableData;

        const slice = data.slice(
            offset,
            offset + perPage
        );

        setPageCount(Math.ceil(data.length / perPage))
        setData(slice)
    }



    const retrieveOrgan = () => {
        axios.get(url).then(json => setData(json.data))

    };

    const refreshList = () => {
        retrieveOrgan();

    };

    const deleteOrgan = (id) => {
        console.log(id);
        axios.delete(`/organs/${id}`)
            .then((result) => {
                if (result.data != null) {
                    alert('Organ été suprimer!!');
                }

                refreshList();
            });

    };

    const CreateOrgane = () => {
        return props.history.push("/adddorgan");
    };





    const renderTable = () => {
        return data.map(con => {
            return (
                <tr>
                    <td>{con.organNom}</td>
                    <ButtonGroup>
                        <Link to={`/editorg/${con.id}`} className="btn btn-sm btn-outline-primary"><FontAwesomeIcon icon={faEdit} /></Link>&nbsp;
                        {/* <Button size="sm" variant="outline-danger" onClick={() => { if (window.confirm('êtes-vous sûr de vouloir supprimer cet élément?')) deleteOrgan(con.id) }}><FontAwesomeIcon icon={faTrash} /></Button>*/}
                    </ButtonGroup>
                </tr>
            )
        })
    }
    return (
        <div>
            <div className="content-wrapper">
                <div>
                    <div className="header bg-primary pb-6">
                        <div className="container-fluid">
                            <div className="header-body">
                                <div className="row align-items-center py-4">
                                    <div className="col-lg-6 col-7">
                                        <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Maladies</h6>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Page content */}
                    <div className="container-fluid mt--6">
                        <div className="row">
                            <div className="col">
                                <div className="card">
                                    {/* Card header */}
                                    <div className="card-header border-0">
                                        <h3 className="mb-0"><FontAwesomeIcon icon={faList} /> Liste des Organs
                                            <Button size="sm" variant="info" type="button" style={{ "width": "150px", "margin": "1px", "padding": "10px", "marginLeft": "500px" }}
                                                onClick={CreateOrgane.bind()}>
                                                <FontAwesomeIcon icon={faPlusSquare} /> Ajouter Organe
                                            </Button>
                                        </h3>
                                    </div>
                                    {/* Light table */}
                                    <div className="table-responsive">
                                        <table class="table table-stripe"  >
                                            <thead className="thead-light">
                                                <tr>
                                                    <th scope="col" className="sort">Nom</th>
                                                    <th scope="col" className="sort">Action </th>
                                                    <th scope="col" />
                                                </tr>
                                            </thead>
                                            <tbody className="list">{renderTable()}</tbody>
                                        </table>
                                        <ReactPaginate
                                            previousLabel={"prev"}
                                            nextLabel={"next"}
                                            breakLabel={"..."}
                                            breakClassName={"break-me"}
                                            pageCount={pageCount}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={handlePageClick}
                                            containerClassName={"pagination"}
                                            subContainerClassName={"pages pagination"}
                                            activeClassName={"active"} />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
