import React, { useEffect, useState } from "react";
import axios from "axios";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlusSquare, faDisease, faSave, faUndo, faList } from "@fortawesome/free-solid-svg-icons";
import { Card, Row, Col, Button } from 'react-bootstrap';

import {
    PageWrapper,
    Title,
    Label,
    Input,
    StyledInlineErrorMessage,
    Submit,
    CodeWrapper
} from "./styles";
import { confirmAlert } from "react-confirm-alert";
import { getOrgan } from "../Examens/Requests";

const formSchema = Yup.object().shape({
    nom: Yup.string()
        .min(2, "C'est trop court")
        .max(200, "C'est trop long")
        .required("Veuillez saisir un nom"),
    code: Yup.string()
        .min(2, "C'est trop court")
        .max(45, "C'est trop long")
        .required("Veuillez saisir un code"),
    catagory: Yup.string()
        .required("Veuillez saisir une catagory"),
    description: Yup.string()
        .min(2, "C'est trop court")
        .max(500, "C'est trop long")
        .required("Veuillez saisir une description"),
    sub_catagory: Yup.string()
        .required("Veuillez saisir une sub catagory"),
    organ: Yup.object().shape({
        id: Yup.string()
            .required("Veuillez saisir un organ")
    })
});
export default function CreateMalady(props) {
    const [errorRequest, setErrorRequest] = useState(false);
    const [organ, setOrgan] = useState([]);
    /* Server State Handling */
    const [serverState, setServerState] = useState();
    const handleServerResponse = (ok, msg) => {
        setServerState({ ok, msg });
    };
    const handleOnSubmit = (values, actions) => {
        confirmAlert({

            title: 'Confirmer pour soumettre',
            message: 'êtes-vous sûr de le faire.',
            buttons: [
                {
                    label: 'Oui',
                    onClick: () => {
                        axios.post("/maladies", values
                        )
                            .then(response => {
                                props.history.push("/listmaladies");
                                actions.setSubmitting(false);
                                actions.resetForm();
                                handleServerResponse(true, "Thanks!");
                                alert("Maladie enregisté avec succés");
                                console.log(response.data);
                            })
                            .catch(error => {
                                actions.setSubmitting(false);
                                handleServerResponse(false, error.response.data.error);
                            });
                        alert('Click Oui')
                    }
                },
                {
                    label: 'Non',
                    onClick: () => alert('Click Non')
                }
            ]
        });

    };

    useEffect(() => {
        async function fetchInitialData() {
            const response = await getOrgan();
            response.errors ? setErrorRequest(true) : setOrgan(response);
        }

        fetchInitialData();
    }, []);
    const [initialValues, setInitialValues] = useState(
        {
             nom: "", code: "", catagory: "", description: "", sub_catagory: "", organ: { id: "" }
        }
    );
    const Liste = () => {
        return props.history.push("/listmaladies");
    };
    const reset = () => {
        setInitialValues(() => initialValues);
    };
    return (
        <div className="container">
            <div className="content-wrapper">
                <div className="header bg-primary pb-6">
                    <div className="container-fluid">
                        <div className="header-body">
                            <div className="row align-items-center py-4">
                                <div className="col-lg-6 col-7">
                                    <h6 className="h2 text-white d-inline-block mb-0"><FontAwesomeIcon icon={faDisease} /> Gestion des Maladies</h6>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container-fluid mt--6">
                    <div className="row">
                        <div className="col">
                            <div className="card">
                                {/* Card header */}
                                <div className="card-header border-0">
                                    <h3 className="mb-0"><FontAwesomeIcon icon={faPlusSquare} /> Ajouter Maladie
                                    </h3>
                                </div>
                                <div className="card-body bg-white">
                                    <Formik
                                        initialValues={initialValues}
                                        onSubmit={handleOnSubmit}
                                        validationSchema={formSchema}
                                    >
                                        {({ isSubmitting,
                                            values,
                                            errors,
                                            touched,
                                            isValidating,
                                            isValid }) => (
                                            <Form id="fs-frm" noValidate>
                                                <Row>
                                                    <Col>

                                                        <Label htmlFor="code">
                                                            Nom
                                            <Input
                                                                type="text"
                                                                name="nom"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Nom"
                                                                valid={touched.nom && !errors.nom}
                                                                error={touched.nom && errors.nom}
                                                            />
                                                        </Label>
                                                        {errors.nom && touched.nom && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.nom}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="code">
                                                            Code
                                            <Input
                                                                type="number"
                                                                name="code"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Code"
                                                                valid={touched.code && !errors.code}
                                                                error={touched.code && errors.code}
                                                            />
                                                        </Label>
                                                        {errors.code && touched.code && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.code}
                                                            </StyledInlineErrorMessage>
                                                        )}

                                                        <Label htmlFor="organ">
                                                            Organ
                                                            <Input id="organ.id" name="organ.id" component="select"
                                                                   valid={touched.organ && !errors.organ}
                                                                   error={touched.organ && errors.organ}
                                                            >
                                                                <option value="">Choisir un Organ</option>
                                                                {organ.map((value) => (
                                                                    <option value={value.id} key={value.id}>
                                                                        {value.organNom}
                                                                    </option>
                                                                ))}
                                                            </Input>
                                                        </Label>


                                                    </Col>
                                                    <Col>

                                                        <Label htmlFor="catagory">
                                                            Catagory
                                            <Input id="catagory" name="catagory" component="select"
                                                                valid={touched.catagory && !errors.catagory}
                                                                error={touched.catagory && errors.catagory}>
                                                                <option value=" " >Choisir une Catagory </option>
                                                                <option value="Maladies de l'oreille, du nez et de la gorge">Maladies de l'oreille, du nez et de la gorge</option>
                                                                <option value="Coeur et système circulatoire">Coeur et système circulatoire</option>
                                                                <option value="Maladies infectieuses">Maladies infectieuses</option>
                                                                <option value="les maladies du sang">les maladies du sang</option>
                                                                <option value="Diabète">Diabète</option>
                                                                <option value="Maladies de la poitrine">Maladies de la poitrine</option>
                                                                <option value="Maladie du foie">Maladie du foie</option>

                                                            </Input>

                                                        </Label>

                                                        <Label htmlFor="sub_catagory">
                                                            Sub Catagory
                                            <Input id="sub_catagory" name="sub_catagory" component="select"
                                                                valid={touched.sub_catagory && !errors.sub_catagory}
                                                                error={touched.sub_catagory && errors.sub_catagory}>
                                                                <option value=" " >Choisir une Sub Catagory </option>
                                                                <option value="L'inflammation de l'oreille moyenne">L'inflammation de l'oreille moyenne</option>
                                                                <option value="Les maladies cardiaques">Les maladies cardiaques</option>
                                                                <option value="Covid 19">Covid 19</option>
                                                                <option value="VIH">VIH</option>
                                                                <option value="Paludisme">Paludisme</option>
                                                                <option value="L'anémie falciforme">L'anémie falciforme</option>
                                                                <option value="Injection Victoza">Injection Victoza</option>
                                                                <option value="Diabète de type 1">Diabète de type 1</option>
                                                                <option value="Diabète de type 2">Diabète de type 2</option>
                                                                <option value="Asthme">Asthme</option>
                                                                <option value="Maladie du foie">Maladie du foie B</option>
                                                                <option value="Maladie du foie">Maladie du foie C</option>
                                                            </Input>

                                                        </Label>
                                                        <Label htmlFor="description">
                                                            Description
                                                            <Input
                                                                component="textarea"
                                                                name="description"
                                                                autoCorrect="off"
                                                                autoComplete="name"
                                                                placeholder="Description"
                                                                valid={touched.description && !errors.description}
                                                                error={touched.description && errors.description}
                                                            />
                                                        </Label>
                                                        {errors.description && touched.description && (
                                                            <StyledInlineErrorMessage>
                                                                {errors.description}
                                                            </StyledInlineErrorMessage>
                                                        )}


                                                    </Col>

                                                </Row>
                                                <Card.Footer style={{ "textAlign": "right" }}>
                                                    <button type="submit" className="btn btn-success"
                                                        style={{ "width": "120px", "margin": "1px", "padding": "2px" }}
                                                        disabled={isSubmitting}>
                                                        <FontAwesomeIcon icon={faSave} /> Enregister
                                        </button>{' '}
                                                    <Button size="sm" variant="info" type="reset" onClick={reset.bind()}>
                                                        <FontAwesomeIcon icon={faUndo} /> Refreche
                                        </Button>{' '}
                                                    <Button size="sm" variant="info" type="button" onClick={Liste.bind()}>
                                                        <FontAwesomeIcon icon={faList} /> liste des Maladies
                                        </Button>
                                                </Card.Footer>
                                                {serverState && (
                                                    <p className={!serverState.ok ? "errorMsg" : ""}>
                                                        {serverState.msg}
                                                    </p>
                                                )}
                                            </Form>
                                        )}
                                    </Formik>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};
