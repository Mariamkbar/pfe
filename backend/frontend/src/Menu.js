import React, { Component } from "react";
import { Link } from "react-router-dom";
import "./App.css";
import {
    faAngleLeft,
    faHospitalUser,
    faUserPlus,
    faListOl,
    faNotesMedical,
    faMicroscope,
    faVial,
    faXRay,
    faDisease, faDiagnoses, faLungs, faPills, faFilePrescription, faUserMd
} from '@fortawesome/free-solid-svg-icons';
import { Nav } from "reactstrap";

import AuthService from "./services/auth.service";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";





export default class Menu extends Component {
    constructor(props) {
        super(props);
        this.logOut = this.logOut.bind(this);

        this.state = {
            showModeratorBoard: false,
            showAdminBoard: false,
            currentUser: undefined,
        };
    }

    componentDidMount() {
        const user = AuthService.getCurrentUser();

        if (user) {
            this.setState({
                currentUser: user,
                showModeratorBoard: user.roles.includes("ROLE_MODERATOR"),
                showAdminBoard: user.roles.includes("ROLE_ADMIN"),
            });
        }
    }
    logOut() {
        AuthService.logout();
    }

    render() {
        const { currentUser, showModeratorBoard, showAdminBoard } = this.state;
        return (
            <div >
                <Nav >
                <aside className="main-sidebar sidebar-light-danger elevation-4">
                    {/* Brand Logo */}
                    <a href="http://localhost:8080/" className="brand-link">
                        <img src="dist/img/logo4.jpeg" className="brand-image img-circle elevation-3"
                            style={{ opacity: '.8' }} />
                        <span className="brand-text font-weight-light"><strong><h1>Docfiles🧪</h1></strong></span>
                    </a>
                    {/* Sidebar */}
                    <div className="sidebar sidebar-dark">
                        {/* Sidebar user panel (optional) */}

                        {/* Sidebar Menu */}
                        <nav className="mt-2">
                            <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                                data-accordion="false">

                                {currentUser && (

                                    <Link to={"/user"} className="nav-link">
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">



                                                <FontAwesomeIcon icon={faHospitalUser} style={{"color":"blue"}} />{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Gérer des patients</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">

                                                <li className="nav-item">
                                                    <Link to="/createp" className="nav-link">
                                                        <FontAwesomeIcon icon={faUserPlus} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong> Ajouter patient</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">

                                                    <Link to="/listpas" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl}  style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Historiques</strong></p>
                                                    </Link>
                                                </li>


                                            </ul>
                                        </li>
                                    </Link>

                                )}


                                {showAdminBoard && (
                                <Link to={"/user"} className="nav-link">
                                    <li className="nav-item has-treeview">
                                        <a href="#" className="nav-link">

                                            <FontAwesomeIcon icon={faNotesMedical} style={{"color":"blue"}} />{' '}
                                            <p style={{
                                                "width": "10px",
                                                "margin": "0px",
                                                "font-size": "17px"
                                            }} >
                                                <strong> Gérer des consultations </strong>
                                                <FontAwesomeIcon icon={faAngleLeft} />

                                            </p>
                                        </a>
                                        <ul className="nav nav-treeview">

                                            <li className="nav-item">
                                                <Link to="/addcons" className="nav-link">
                                                    <FontAwesomeIcon icon={faNotesMedical} style={{"color":"blue"}} />{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong> Ajouter consultation</strong></p>
                                                </Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link to="/addEcons" className="nav-link">
                                                    <FontAwesomeIcon icon={faVial} style={{"color":"blue"}} />{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong> Consulter examen</strong></p>
                                                </Link>
                                            </li>
                                            <li className="nav-item">
                                                <Link to="/addDcons" className="nav-link">
                                                    <FontAwesomeIcon icon={faDiagnoses} style={{"color":"blue"}} />{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong>  Consulter diagnostic</strong></p>
                                                </Link>
                                            </li>

                                            <li className="nav-item">
                                                <Link to="/addOcons" className="nav-link">
                                                    <FontAwesomeIcon icon={faFilePrescription}  style={{"color":"blue"}}/>{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong> Consulter ordonance</strong></p>
                                                </Link>
                                            </li>

                                            <li className="nav-item">
                                                <Link to="/addScons" className="nav-link">
                                                    <FontAwesomeIcon icon={faNotesMedical} style={{"color":"blue"}}/>{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong>Simple consultation </strong></p>
                                                </Link>
                                            </li>


                                            <li className="nav-item">

                                                <Link to="/listcons" className="nav-link">
                                                    <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}}/>{' '}
                                                    <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                    <p style={{ "font-size": "15px" }}><strong>Historiques</strong></p>
                                                </Link>
                                            </li>

                                        </ul>
                                    </li>
                                </Link>
                                )}




                                {showAdminBoard && (

                                    <Link to={"/admin"} className="nav-link">

                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faMicroscope}  style={{"color":"blue"}}/>{' '}
                                                <p style={{ "font-size": "17px" }}>

                                                    <strong>Demande d'examens </strong>
                                                    <FontAwesomeIcon icon={faAngleLeft}  />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">
                                                <li className="nav-item">
                                                    <Link to="/addexamans" className="nav-link">
                                                        <FontAwesomeIcon icon={faVial} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Ajouter examen</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">
                                                    <Link to="/listexam" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste examens</strong></p>
                                                    </Link>
                                                </li>


                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faXRay}  style={{"color":"blue"}}/>{' '}
                                                <p style={{ "font-size": "17px" }}>

                                                    <p style={{ "font-size": "14px" }}><strong>Gérer les résultats d'examens </strong></p>
                                                    <FontAwesomeIcon icon={faAngleLeft}  />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">
                                                <li className="nav-item">
                                                    <Link to="/adddcpe" className="nav-link">
                                                        <FontAwesomeIcon icon={faXRay} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong> Ajouter résultat d'examens</strong> </p>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link to="/addres" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl}  style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste résultats</strong></p>
                                                    </Link>
                                                </li>
                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faDisease}  style={{"color":"blue"}}/>{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Gérer des maladies</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">
                                                <li className="nav-item">
                                                    <Link to="/addmaladies" className="nav-link">
                                                        <FontAwesomeIcon icon={faDisease} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Ajouter maladie</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">
                                                    <Link to="/adddorgan" className="nav-link">
                                                        <FontAwesomeIcon icon={faLungs} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Ajouter organ</strong></p>
                                                    </Link>
                                                </li>
                                                <li className="nav-item">
                                                    <Link to="/listmaladies" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste maladies</strong></p>
                                                    </Link>
                                                </li>


                                                <li className="nav-item">
                                                    <Link to="listorgan" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl}  style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste organs</strong></p>
                                                    </Link>
                                                </li>

                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faDiagnoses}  style={{"color":"blue"}}/>{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Faire des diagnostics</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">

                                                <li className="nav-item">
                                                    <Link to="/adddg" className="nav-link">
                                                        <FontAwesomeIcon icon={faDiagnoses} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong> Ajouter diagnostic</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">
                                                    <Link to="/listDiag" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste diagnostics</strong></p>
                                                    </Link>
                                                </li>

                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">

                                                <FontAwesomeIcon icon={faUserMd} style={{"color":"blue"}}/>{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Gérer des médecins</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">

                                                <li className="nav-item">
                                                    <Link to="/addmedecins" className="nav-link">
                                                        <FontAwesomeIcon icon={faUserPlus} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Ajouter médecin</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">

                                                    <Link to="/listmedecins" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Historiques</strong></p>
                                                    </Link>
                                                </li>


                                            </ul>
                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faPills} style={{"color":"blue"}} />{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Gérer des médicaments</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />

                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">
                                                <li className="nav-item">
                                                    <Link to="/adddrugs" className="nav-link">
                                                        <FontAwesomeIcon icon={faPills} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong> Ajouter médicament</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">
                                                    <Link to="/listdrugs" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Liste médicaments</strong></p>
                                                    </Link>
                                                </li>



                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">
                                                <FontAwesomeIcon icon={faFilePrescription} style={{"color":"blue"}} />{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Donner des ordonnances</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />

                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">

                                                <li className="nav-item">
                                                    <Link to="/adddpr" className="nav-link">
                                                        <FontAwesomeIcon icon={faFilePrescription} style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Ajouter ordonnance</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">
                                                    <Link to="/listpr" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "14px" }}><strong>Liste ordonnances</strong></p>
                                                    </Link>
                                                </li>


                                            </ul>

                                        </li>
                                        <li className="nav-item has-treeview">
                                            <a href="#" className="nav-link">



                                                <FontAwesomeIcon icon={faUserPlus} style={{"color":"blue"}} />{' '}
                                                <p style={{ "font-size": "17px" }}>
                                                    <strong>Gérer les utilisateurs</strong>
                                                    <FontAwesomeIcon icon={faAngleLeft} />
                                                </p>
                                            </a>
                                            <ul className="nav nav-treeview">

                                                <li className="nav-item">
                                                    <Link to="/register" className="nav-link">
                                                        <FontAwesomeIcon icon={faUserPlus} style={{"color":"blue"}} />{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong> Ajouter utilisateur</strong></p>
                                                    </Link>
                                                </li>

                                                <li className="nav-item">

                                                    <Link to="/userliste" className="nav-link">
                                                        <FontAwesomeIcon icon={faListOl}  style={{"color":"blue"}}/>{' '}
                                                        <span className="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                                                        <p style={{ "font-size": "15px" }}><strong>Historiques</strong></p>
                                                    </Link>
                                                </li>


                                            </ul>
                                        </li>

                                    </Link>

                                )}




                            </ul>
                        </nav>

                        {/* /.sidebar-menu */}
                    </div>
                    {/* /.sidebar */}
                </aside>
                    </Nav>
            </div>


        )
    }
}
