package com.docfiles.repository;

import com.docfiles.models.Conducted_physical_exam;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;


public interface Conducted_physical_examRepository extends MongoRepository<Conducted_physical_exam, String> {
    @Override
    void delete (Conducted_physical_exam deleted);


    Optional<Conducted_physical_exam> findById(String id);
}
