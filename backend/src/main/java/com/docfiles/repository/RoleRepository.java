package com.docfiles.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.docfiles.models.ERole;
import com.docfiles.models.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
  Optional<Role> findByName(ERole name);
}
