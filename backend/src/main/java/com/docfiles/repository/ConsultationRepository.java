package com.docfiles.repository;

import com.docfiles.models.Consultation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


public interface ConsultationRepository extends MongoRepository<Consultation, String> {
    List<Consultation> findByTitre(String titre);

    @Query("{ 'Patient.id' :?0}")
    List<Consultation> findByPatientId(String id);

}
