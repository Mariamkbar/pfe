package com.docfiles.repository;

import com.docfiles.models.Prescribe;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface PrescribeRepository extends MongoRepository<Prescribe, String> {


}
