package com.docfiles.repository;


import com.docfiles.models.Organ;
import org.springframework.data.mongodb.repository.MongoRepository;


public interface OrganRepository extends MongoRepository<Organ, String> {


}
