package com.docfiles.repository;

import com.docfiles.models.Physical_exam;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface Physical_examRepository extends MongoRepository<Physical_exam, String> {


}
