package com.docfiles.repository;

import com.docfiles.models.Medecin;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface MedecinRepository extends MongoRepository<Medecin, String> {

}

