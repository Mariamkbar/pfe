package com.docfiles.repository;


import com.docfiles.models.Diagnostic;

import org.springframework.data.mongodb.repository.MongoRepository;




public interface DiagnosticRepository extends MongoRepository<Diagnostic, String> {

}
