package com.docfiles.repository;

import com.docfiles.models.Patient;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface PatientRepository extends MongoRepository<Patient, String> {



}
