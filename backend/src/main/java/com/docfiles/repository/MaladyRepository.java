package com.docfiles.repository;


import com.docfiles.models.Malady;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface MaladyRepository extends MongoRepository<Malady, String> {


}

