package com.docfiles.repository;



import com.docfiles.models.Drug;
import org.springframework.data.mongodb.repository.MongoRepository;



public interface DrugRepository extends MongoRepository<Drug, String> {

}
