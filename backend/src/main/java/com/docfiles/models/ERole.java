package com.docfiles.models;

public enum ERole {//les types des Roles des utilisateurs
  ROLE_USER,
  ROLE_MODERATOR,
  ROLE_ADMIN
}
