package com.docfiles.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "diagnostics")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Diagnostic {//diagnostiques Document
    @Id
    private String id;
    private String Comment;
    private String nom;

    @DBRef
    private Malady malady;


}
