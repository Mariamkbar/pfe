package com.docfiles.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;

@Document(collection = "organs")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Organ {//organe Document

    public Organ(String organNom) {
        this.organNom = organNom;
    }

    @Id
    private String id;
    private String organNom;

}
