package com.docfiles.models;


import com.mongodb.lang.NonNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "drugs")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Drug {//Médicament Document
    @Id
    @NonNull
    private String id;

    public Drug(String nom, String brand, String indication, String contraindication) {
        this.nom = nom;
        this.brand = brand;
        this.indication = indication;
        this.contraindication = contraindication;
    }

    private String nom;
    private String brand;
    private String indication;
    private String contraindication;



    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setIndication(String indication) {
        this.indication = indication;
    }

    public void setContraindication(String contraindication) {
        this.contraindication = contraindication;
    }

    public String getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public String getBrand() {
        return brand;
    }

    public String getIndication() {
        return indication;
    }

    public String getContraindication() {
        return contraindication;
    }
}
