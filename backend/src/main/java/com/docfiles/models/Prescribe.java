package com.docfiles.models;



import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "prescribes")
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Prescribe {//Ordonnance Document
    @Id
    private String id;
    private String nom;
    private String dosage;
    private Long duration;


    @DBRef
    private List<Drug> drugs;
}


