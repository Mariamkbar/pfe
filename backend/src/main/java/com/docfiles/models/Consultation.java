package com.docfiles.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document(collection = "consultations")
@Data
@AllArgsConstructor @NoArgsConstructor
@ToString
public class Consultation {//Consultation Document
    @Id
    private String id;
    private String titre;
    private String observation;
    private LocalDate date;
    private Long  duration;
    private Double cost;
    private String diagnostic;
    private String status;

    //les références des Objets

    @DBRef
    private Patient patient;
    @DBRef
    private Medecin medecin;
    @DBRef
    private Prescribe prescribe;
    @DBRef
    private Diagnostic diagnostics;
    @DBRef
    private Conducted_physical_exam conducted_physical_exam;
}
