package com.docfiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocfilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DocfilesApplication.class, args);
	}

}
