package com.docfiles.controllers;


import com.docfiles.models.Drug;
import com.docfiles.repository.DrugRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DrugController {

    @Autowired
    DrugRepository drugRepository;

    @RequestMapping(method= RequestMethod.GET, value="/drugs")
    public Iterable<Drug> drug() {
        return drugRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/drugs")
    public Drug save(@RequestBody Drug drug) {
        drugRepository.save(drug);

        return drug;
    }

    @RequestMapping(method=RequestMethod.GET, value="/drugs/{id}")
    public Optional<Drug> show(@PathVariable String id) {
        return drugRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/drugs/{id}")
    public Drug update(@PathVariable String id, @RequestBody Drug drug) {
        Optional<Drug> optionalDrug = drugRepository.findById(id);
        Drug d = optionalDrug.get();
        if(drug.getNom() != null)
            d.setNom(drug.getNom());
        if(drug.getBrand() != null)
            d.setBrand(drug.getBrand());
        if(drug.getIndication() != null)
            d.setIndication(drug.getIndication());
        if(drug.getContraindication() != null)
            d.setContraindication(drug.getContraindication());
        drugRepository.save(d);
        return d;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/drugs/{id}")
    public String delete(@PathVariable String id) {
        Optional<Drug> optionalDrug = drugRepository.findById(id);
        Drug drug = optionalDrug.get();
        drugRepository.delete(drug);

        return "";
    }
}
