package com.docfiles.controllers;


import com.docfiles.models.Organ;
import com.docfiles.models.Physical_exam;
import com.docfiles.repository.OrganRepository;
import com.docfiles.repository.Physical_examRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class Physical_examContrller {

    @Autowired
    Physical_examRepository physical_examRepository;

    @Autowired
    OrganRepository organRepository;

    @RequestMapping(method= RequestMethod.GET, value="/physical_exams")
    public Iterable<Physical_exam> physical_exams() {
        return physical_examRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/physical_exams")
    public Physical_exam save(@RequestBody Physical_exam physical_exam) {
        physical_examRepository.save(physical_exam);

        return physical_exam;
    }

    @RequestMapping(method=RequestMethod.GET, value="/physical_exams/{id}")
    public Optional<Physical_exam> show(@PathVariable String id) {
        return physical_examRepository.findById(id);
    }



    @RequestMapping(method=RequestMethod.PATCH, value="/physical_exams/{id}") public Physical_exam update(@PathVariable("id") String id, @RequestBody Physical_exam physical_exam,String organenom) {

        Optional<Physical_exam> optionalPhysical_exam = physical_examRepository.findById(id);

        Physical_exam ph = optionalPhysical_exam.get();
        if(physical_exam.getNom() != null)
            ph.setNom(physical_exam.getNom());

        if(physical_exam.getCatagory() != null)
            ph.setCatagory(physical_exam.getCatagory());

        if(physical_exam.getSub_catagory() != null)
            ph.setSub_catagory(physical_exam.getSub_catagory());

        if(physical_exam.getDescription() != null)
            ph.setDescription(physical_exam.getDescription());

        if(physical_exam.getUnit() != null)
            ph.setUnit(physical_exam.getUnit());

        if(physical_exam.getUnit_label() != null)
            ph.setUnit_label(physical_exam.getUnit_label());

        if(physical_exam.getUnit_type() != null)
            ph.setUnit_type(physical_exam.getUnit_type());
        if(physical_exam.getOrgan() != null)
            ph.setOrgan(physical_exam.getOrgan());


        physical_examRepository.save(ph);
        return ph;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/physical_exams/{id}")
    public String delete(@PathVariable String id) {
        Optional<Physical_exam> optionalPhysical_exam = physical_examRepository.findById(id);
        Physical_exam physical_exam = optionalPhysical_exam.get();
        physical_examRepository.delete(physical_exam);

        return "";
    }

}
