package com.docfiles.controllers;



import com.docfiles.models.*;
import com.docfiles.repository.ConsultationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
public class ConsultationController {

    @Autowired
    ConsultationRepository consultationRepository;
    @RequestMapping(method= RequestMethod.GET, value="/consultations")
    public Iterable<Consultation> consultation() {
        return consultationRepository.findAll();
    }

    @RequestMapping(method= RequestMethod.GET, value="/consultations/{id}")
    public Optional<Consultation> show(@PathVariable String id) {
        return consultationRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.POST, value="/consultations")
    public Consultation save(@RequestBody Consultation consultation) {
        consultationRepository.save(consultation);

        return consultation;
    }

    @RequestMapping(method=RequestMethod.PATCH, value="/consultations/{id}")
    public Consultation update(@PathVariable String id, @RequestBody Consultation consultation) {
        Optional<Consultation> optionalConsultation= consultationRepository.findById(id);
        Consultation c = optionalConsultation.get();
        if(consultation.getTitre() != null)
            c.setTitre(consultation.getTitre());

        if(consultation.getObservation() != null)
            c.setObservation(consultation.getObservation());

        if(consultation.getDate() != null)
            c.setDate(consultation.getDate());

        if(consultation.getDuration() != null)
            c.setDuration(consultation.getDuration());

        if(consultation.getCost() != null)
            c.setCost(consultation.getCost());

        if(consultation.getDiagnostic() != null)
            c.setDiagnostic(consultation.getDiagnostic());

        if(consultation.getStatus() != null)
            c.setStatus(consultation.getStatus());

        /*if(consultation.getMedecin() != null)
            c.setMedecin(consultation.getMedecin());

        if(consultation.getPrescribe() != null)
            c.setPrescribe(consultation.getPrescribe());

        if(consultation.getDiagnostics() != null)
            c.setDiagnostics(consultation.getDiagnostics());

        if(consultation.getConducted_physical_exam() != null)
            c.setConducted_physical_exam(consultation.getConducted_physical_exam());*/




        consultationRepository.save(c);
        return c;
    }


    @RequestMapping(method=RequestMethod.DELETE, value="/consultations/{id}")
    public String delete(@PathVariable String id) {
        Optional<Consultation> optionalConsultation = consultationRepository.findById(id);
        Consultation consultation = optionalConsultation.get();
        consultationRepository.delete(consultation);

        return "";
    }

    @RequestMapping(method= RequestMethod.GET, value="/consultations/getConsultationTitre/{titre}")
    public List<Consultation> getConsultationTitre(@PathVariable String titre){
        return consultationRepository.findByTitre(titre);
    }

    @RequestMapping(method= RequestMethod.GET, value="/consultations/getConsultationByPatientId/{id}")
    public List<Consultation> getConsultationByPatientId(@PathVariable String id){
        return consultationRepository.findByPatientId(id);
    }






}
