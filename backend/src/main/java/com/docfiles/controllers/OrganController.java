package com.docfiles.controllers;


import com.docfiles.models.Organ;
import com.docfiles.repository.OrganRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class OrganController {

    @Autowired
    OrganRepository organRepository;
    @RequestMapping(method= RequestMethod.GET, value="/organs")
    public Iterable<Organ> organ() {
        return organRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/organs")
    public Organ save(@RequestBody Organ organ) {
        organRepository.save(organ);

        return organ;
    }

    @RequestMapping(method=RequestMethod.GET, value="/organs/{id}")
    public Optional<Organ> show(@PathVariable String id) {
        return organRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/organs/{id}")
    public Organ update(@PathVariable String id, @RequestBody Organ organ) {
        organ.setId(id);
        organRepository.save(organ);
        return organ;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/organs/{id}")
    public String delete(@PathVariable String id) {
        Optional<Organ> optionalOrgan = organRepository.findById(id);
        Organ organ = optionalOrgan.get();
        organRepository.delete(organ);

        return "";
    }

}
