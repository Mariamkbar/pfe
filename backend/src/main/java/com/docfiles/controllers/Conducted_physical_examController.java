package com.docfiles.controllers;


import com.docfiles.models.Conducted_physical_exam;
import com.docfiles.models.Physical_exam;
import com.docfiles.repository.Conducted_physical_examRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;
@RestController
public class Conducted_physical_examController {


    @Autowired
    Conducted_physical_examRepository conducted_physical_examRepository;


    @RequestMapping(method= RequestMethod.GET, value="/conducted_physical_exams")
    public Iterable<Conducted_physical_exam> organ() {
        return conducted_physical_examRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/conducted_physical_exams")
    public Conducted_physical_exam save(@RequestBody Conducted_physical_exam conducted_physical_exam) {
        conducted_physical_examRepository.save(conducted_physical_exam);

        return conducted_physical_exam;
    }

    @RequestMapping(method=RequestMethod.GET, value="/conducted_physical_exams/{id}")
    public Optional<Conducted_physical_exam> show(@PathVariable String id) {
        return conducted_physical_examRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/conducted_physical_exams/{id}")
    public Conducted_physical_exam update(@PathVariable String id, @RequestBody Conducted_physical_exam conducted_physical_exam, Physical_exam physical_exam) {
        Optional<Conducted_physical_exam> optionalConducted_physical_exam = conducted_physical_examRepository.findById(id);
        Conducted_physical_exam cn = optionalConducted_physical_exam.get();
        if(conducted_physical_exam.getResult() != null)
            cn.setResult(conducted_physical_exam.getResult());
        if(conducted_physical_exam.getComment() != null)
            cn.setComment(conducted_physical_exam.getComment());
        conducted_physical_examRepository.save(cn);
        return cn;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/conducted_physical_exams/{id}")
    public String delete(@PathVariable String id) {
        Optional<Conducted_physical_exam> optionalConducted_physical_exam = conducted_physical_examRepository.findById(id);
        Conducted_physical_exam conducted_physical_exam = optionalConducted_physical_exam.get();
        conducted_physical_examRepository.delete(conducted_physical_exam);

        return "";
    }


}
