package com.docfiles.controllers;


import com.docfiles.models.Consultation;
import com.docfiles.models.Drug;
import com.docfiles.models.Organ;
import com.docfiles.models.Prescribe;
import com.docfiles.repository.DiagnosticRepository;
import com.docfiles.repository.DrugRepository;
import com.docfiles.repository.PrescribeRepository;
import com.jayway.jsonpath.Criteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import java.util.List;
import java.util.Optional;

@RestController
public class PrescribeController {

    @Autowired
    PrescribeRepository prescribeRepository;

    @Autowired
    DrugRepository drugRepository;

    @RequestMapping(method= RequestMethod.GET, value="/prescribes")
    public Iterable<Prescribe> prescribe() {
        return prescribeRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/prescribes")
    public Prescribe save(@RequestBody Prescribe prescribe) {
        prescribeRepository.save(prescribe);

        return prescribe;
    }



    @RequestMapping(method=RequestMethod.GET, value="/prescribes/{id}")
    public Optional<Prescribe> show(@PathVariable String id) {
        return prescribeRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/prescribes/{id}")
    public Prescribe update(@PathVariable String id, @RequestBody Prescribe prescribe) {
        Optional<Prescribe> optionalPrescribe = prescribeRepository.findById(id);
        Prescribe p = optionalPrescribe.get();
        /*if(prescribe.getDrugs()!= null){
            p.setDrugs(prescribe.getDrugs());
        }*/
        if(prescribe.getDosage() != null)
            p.setDosage(prescribe.getDosage());

        if(prescribe.getDuration() != null)
        p.setDuration(prescribe.getDuration());

        if(prescribe.getNom() != null)
            p.setNom(prescribe.getNom());


        prescribeRepository.save(p);
        return p;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/prescribes/{id}")
    public String delete(@PathVariable String id) {
        Optional<Prescribe> optionalPrescribe = prescribeRepository.findById(id);
        Prescribe prescribe = optionalPrescribe.get();
        prescribeRepository.delete(prescribe);

        return "";
    }


}
