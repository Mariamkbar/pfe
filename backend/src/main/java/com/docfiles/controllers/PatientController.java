package com.docfiles.controllers;


import com.docfiles.models.Patient;
import com.docfiles.repository.PatientRepository;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
public class PatientController {

    @Autowired
    PatientRepository patientRepository;


    @RequestMapping(method= RequestMethod.GET, value="/patients")
    public Iterable<Patient> getALL() {
        return patientRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/patients")
    Patient createPatient(String id, String nom, String prenom, String sexe, @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate birth_date, String birth_place, String nationalite,
                          String ville, String contry, String adress_1, String adress_2, String tel1, String tel2, String groupe_sanguin,
                          String profession, Long birth_rank, String whatapp, String wire, MultipartFile imgs
    ) throws IOException {
        Patient patient = new Patient();
        patient.setId(id);
        patient.setNom(nom);
        patient.setPrenom(prenom);
        patient.setSexe(sexe);
        patient.setBirth_date(birth_date);
        patient.setBirth_place(birth_place);
        patient.setNationalite(nationalite);
        patient.setVille(ville);
        patient.setContry(contry);
        patient.setAdress_1(adress_1);
        patient.setAdress_2(adress_2);
        patient.setTel1(tel1);
        patient.setTel2(tel2);
        patient.setGroupe_sanguin(groupe_sanguin);
        patient.setProfession(profession);
        patient.setBirth_rank(birth_rank);
        patient.setWhatapp(whatapp);
        patient.setWire(wire);
        if(imgs != null){
            System.out.println("image est ajouter");
            patient.setImage(new Binary(imgs.getBytes()));}
        else {
            System.out.println("image n'est pas ajouter");
        }
        //patient.setImage(new Binary(imgs.getBytes()));
        return patientRepository.save(patient);
    }

    @RequestMapping(method= RequestMethod.GET, value="/patients/image")
    String getImage(@RequestParam String id) {
        Optional<Patient> patient = patientRepository.findById(id);
        Base64.Encoder encoder = Base64.getEncoder();

        return encoder.encodeToString(patient.get().getImage().getData());

    }

    @RequestMapping(method= RequestMethod.GET, value="/image-text")
    String getImageText(@RequestParam String id) {
        Optional<Patient> patient = patientRepository.findById(id);
        ITesseract instance = new Tesseract();
        try {
            ByteArrayInputStream bais = new ByteArrayInputStream(patient.get().getImage().getData());
            BufferedImage bufferImg = ImageIO.read(bais);
            String imgText = instance.doOCR(bufferImg);
            return imgText;
        } catch (Exception e) {
            return "Error while reading image";
        }
    }

    @RequestMapping(method= RequestMethod.GET, value="/patients/{id}")
    public Optional<Patient> show(@PathVariable String id) {
        return patientRepository.findById(id);
    }



    @RequestMapping(method=RequestMethod.PUT, value="/patients/{id}")
    public Patient update(@PathVariable String id, @RequestBody Patient patient) {
        Optional<Patient> optionalPatient = patientRepository.findById(id);
        Patient p = optionalPatient.get();
        if(patient.getNom() != null)
            p.setNom(patient.getNom());

        if(patient.getPrenom() != null)
            p.setPrenom(patient.getPrenom());

        if(patient.getSexe() != null)
            p.setSexe(patient.getSexe());
        if(patient.getBirth_date() != null)
            p.setBirth_date(patient.getBirth_date());
        if(patient.getBirth_place() != null)
            p.setBirth_place(patient.getBirth_place());

        if(patient.getNationalite() != null)
            p.setNationalite(patient.getNationalite());

        if(patient.getVille() != null)
            p.setVille(patient.getVille());
        if(patient.getContry() != null)
            p.setContry(patient.getContry());

        if(patient.getAdress_1() != null)
            p.setAdress_1(patient.getAdress_1());

        if(patient.getAdress_2() != null)
            p.setAdress_2(patient.getAdress_2());


        if(patient.getTel1() != null)
            p.setTel1(patient.getTel1());

        if(patient.getTel2() != null)
            p.setTel2(patient.getTel2());

        if(patient.getGroupe_sanguin() != null)
            p.setGroupe_sanguin(patient.getGroupe_sanguin());
        if(patient.getProfession() != null)
            p.setProfession(patient.getProfession());

        if(patient.getBirth_rank() != null) {
            p.setBirth_rank(patient.getBirth_rank());
        }

        if(patient.getWhatapp() != null)
            p.setWhatapp(patient.getWhatapp());

        if(patient.getWire() != null)
            p.setWire(patient.getWire());

        if(patient.getImage() != null)
            p.setImage(patient.getImage());

        patientRepository.save(p);
        return p;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/patients/{id}")
    public String delete(@PathVariable String id) {
        Optional<Patient> optionalPatient = patientRepository.findById(id);
        Patient patient = optionalPatient.get();
        patientRepository.delete(patient);

        return "";
    }


}