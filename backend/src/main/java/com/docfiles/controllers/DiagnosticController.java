package com.docfiles.controllers;

import com.docfiles.models.Diagnostic;
import com.docfiles.repository.DiagnosticRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DiagnosticController {

    @Autowired
    DiagnosticRepository diagnosticRepository;

    @RequestMapping(method= RequestMethod.GET, value="/diagnostics")
    public Iterable<Diagnostic> diagnostic() {
        return diagnosticRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/diagnostics")
    public Diagnostic save(@RequestBody Diagnostic diagnostic) {
        diagnosticRepository.save(diagnostic);

        return diagnostic;
    }

    @RequestMapping(method=RequestMethod.GET, value="/diagnostics/{id}")
    public Optional<Diagnostic> show(@PathVariable String id) {
        return diagnosticRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/diagnostics/{id}")
    public Diagnostic update(@PathVariable String id, @RequestBody Diagnostic diagnostic) {
        Optional<Diagnostic> optionalDiagnostic = diagnosticRepository.findById(id);
        Diagnostic d = optionalDiagnostic.get();
        if(diagnostic.getComment() != null)
            d.setComment(diagnostic.getComment());
        if(diagnostic.getNom() != null)
            d.setNom(diagnostic.getNom());
        diagnosticRepository.save(d);
        return d;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/diagnostics/{id}")
    public String delete(@PathVariable String id) {
        Optional<Diagnostic> optionalDiagnostic = diagnosticRepository.findById(id);
        Diagnostic diagnostic = optionalDiagnostic.get();
        diagnosticRepository.delete(diagnostic);

        return "";
    }
}
