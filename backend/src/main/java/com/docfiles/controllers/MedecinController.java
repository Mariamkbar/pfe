package com.docfiles.controllers;

import com.docfiles.models.Medecin;
import com.docfiles.repository.ConsultationRepository;
import com.docfiles.repository.MedecinRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
@RestController
public class MedecinController {

    @Autowired
    MedecinRepository medecinRepository;
    @Autowired
    ConsultationRepository consultationRepository;

    @RequestMapping(method= RequestMethod.GET, value="/medecins")
    public List<Medecin> medecin() {
        return medecinRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/medecins")
    public Medecin save(@RequestBody Medecin medecin) {
        medecinRepository.save(medecin);

        return medecin;
    }

    @RequestMapping(method=RequestMethod.GET, value="/medecins/{id}")
    public Optional<Medecin> show(@PathVariable String id) {
        return medecinRepository.findById(id);
    }

    @RequestMapping(method=RequestMethod.PUT, value="/medecins/{id}")
    public Medecin update(@PathVariable String id, @RequestBody Medecin medecin) {
        Optional<Medecin> optionalMedecin= medecinRepository.findById(id);
        Medecin m = optionalMedecin.get();
        if(medecin.getNom() != null)
            m.setNom(medecin.getNom());

        if(medecin.getSpecialite() != null)
            m.setSpecialite(medecin.getSpecialite());

        if(medecin.getAddress() != null)
            m.setAddress(medecin.getAddress());

        if(medecin.getTel() != null)
            m.setTel(medecin.getTel());

        medecinRepository.save(m);
        return m;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/medecins/{id}")
    public String delete(@PathVariable String id) {
        Optional<Medecin> optionalMedecin = medecinRepository.findById(id);
        Medecin medecin = optionalMedecin.get();
        medecinRepository.delete(medecin);

        return "";
    }
}
