package com.docfiles.controllers;


import com.docfiles.models.Malady;
import com.docfiles.repository.MaladyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class MaladyController {

    @Autowired
    MaladyRepository maladyRepository;


    @RequestMapping(method= RequestMethod.GET, value="/maladies")
    public Iterable<Malady> maladie() {
        return maladyRepository.findAll();
    }

    @RequestMapping(method=RequestMethod.POST, value="/maladies")
    public Malady save(@RequestBody Malady malady) {
        maladyRepository.save(malady);

        return malady;
    }

    @RequestMapping(method=RequestMethod.GET, value="/maladies/{id}")
    public Optional<Malady> show(@PathVariable String id) {
        return maladyRepository.findById(id);
    }


    @RequestMapping(method=RequestMethod.PUT, value="/maladies/{id}")
    public Malady update(@PathVariable String id, @RequestBody Malady malady) {
        Optional<Malady> optionalMalady = maladyRepository.findById(id);
        Malady m = optionalMalady.get();
        if(malady.getNom() != null)
            m.setNom(malady.getNom());
        if(malady.getCode() != null)
            m.setCode(malady.getCode());
        if(malady.getCatagory() != null)
            m.setCatagory(malady.getCatagory());
        if(malady.getDescription() != null)
            m.setDescription(malady.getDescription());
        if(malady.getSub_catagory() != null)
            m.setSub_catagory(malady.getSub_catagory());
        /*if(malady.getOrgan() != null)
            m.setOrgan(malady.getOrgan());*/
        maladyRepository.save(m);
        return m;
    }

    @RequestMapping(method=RequestMethod.DELETE, value="/maladies/{id}")
    public String delete(@PathVariable String id) {
        Optional<Malady> optionalMalady = maladyRepository.findById(id);
        Malady malady = optionalMalady.get();
        maladyRepository.delete(malady);

        return "";
    }
}
